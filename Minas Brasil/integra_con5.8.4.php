<?php                                                                                          

/* ############################################################################    
##                                                                           ##
##   APLICATIVO PARA INTEGRAÇÃO EMPORIUM X CONSINCO VIA ORACLE               ##
##   INTEGRA_CON5.PHP : PROCESSAMENTO AUTOMATICO DE EXPORTACAO DE MOVIMENTO  ##
##   DATA             : 21 DE MAIO DE 2014                                   ##
##   AUTORES          : EDNILSON RAFAEL DE CARVALHO                          ##
##                    : JARISON ARAUJO                                       ##
##   VERSAO           : 8.4 - 06/06/2016                                     ##
##                                                                           ##
###############################################################################*/                                                                


/* ##########################################################
   ################                          ################
   ################  BEGIN APLICATION MAIN   ################
   ################                          ################
   ########################################################## */


// CONFIGURACAO DO SISTEMA
 $ini = LoadingConfiguration("/var/emporium/excribo/bin/integra_con5.ini");
 if($ini == -1){
   LogMsg ("ARQUIVO DE CONFIGURACAO NAO ENCONTRADO");
   exit;
 }

// PARAMETROS DE ENTRADA  
$store               = $argv[1];                                                // NRO. LOJA
$pos                 = $argv[2];                                                // NRO. PDV
$ticket              = $argv[3];                                                // NRO. CUPOM                               
$date                = $argv[4];                                                // DATA
$file_name           = $argv[5];                                                // NOME ARQUIVO
$VERSION             = '8.4' ;                                                  // VERSAO ATUAL
$DATABASE            = $ini["MYSQLDB"];                                         // BANCO DE DADOS
$CUSTOMER_CARD       = FALSE;                                                   // GARCIA IGUAL A TRUE
$AGREEMENT_MEDIA     = 4;                                                       // FINALIZADORA CONVENIO
$PBM_MEDIA           = 18;                                                      // FINALIZADORA PBM
$TYPE_CARTDEB_RC     = 511;                                                     // CODIGO MOV. TESOURARIA RECARGA CART.DEBITO
$TYPE_CARTCRED_RC    = 512;                                                     // CODIGO MOV. TESOURARIA RECARGA CART.CREDITO
$START_RECORD_ORACLE = TRUE;                                                    // INICIAR GRAVACAO ORACLE
$START_RECORD_TXT    = TRUE;                                                    // INICIAR GRAVACAO TXT
$START_NOTIFICATION  = FALSE;                                                    // INICIAR GRAVACAO DE ALERTAS NA NOTIFICACAO 
$START_SEND_EMAIL    = TRUE;                                                    // INICIAR ENVIO DE ALERTAS POR EMAIL

// REGISTROS DE LOG
LogMsg ("====================================================================");
LogMsg ("VERSAO.....: $VERSION   ");
LogMsg ("DATA.......: $date      ");
LogMsg ("LOJA.......: $store     ");
LogMsg ("PDV........: $pos       ");
LogMsg ("TICKET.....: $ticket    ");
LogMsg ("ARQUIVO....: $file_name ");
                                              
// STATUS DA LOJA NA INTEGRACAO
$integration = SelectIntegrationCon5($store);
if($integration["status"] == 'Inativo'){
  $START_RECORD_ORACLE = FALSE;                                                 // PARANDO GRAVACAO ORACLE
  LogMsg("lOJA  $store  DESABILITADA NA INTEGRACAO ");
  LogMsg("PARANDO GRAVACAO ORACLE :: XML ::".$file_name);
}

LogMsg ("CARREGANDO ARQUIVO XML ::: $file_name");
$sFile = file_get_contents($file_name);                                         // CONVERTE PARA STRING
$aFile = file($file_name);                                                      // CONVERTE PARA ARRAY

$fiscal_day = ReadXML($aFile, "<FISCAL_DAY>");                                  // DATA FISCAL 
LogMsg ("DATA FISCAL: ".$fiscal_day);

// REINICIANDO INTEGRACAO DA LOJA 
if(substr_count ($sFile, "<BEGIN_INTEGRATION>") > 0 ) {
  LogMsg("** INICIANDO INTEGRACAO CON5 PARA LOJA $store ** ");
  //RestartIntegration ( $store, $fiscal_day);   //validando rotina 10/12                                                        
  
  $sql  = "UPDATE integration_con5 SET ";
  $sql .= "status = 'Ativo', ";
  $sql .= "start = '".date("Y-m-d H:i:s")."', ";
  $sql .= "finish = NULL, ";
  $sql .= "msg = NULL ";
  $sql .= "WHERE store_key = $store ";
  $data = ConnectingInMysql($sql, 3);
  
  LogMsg("** LOJA $store ATIVADA NA INTEGRACAO CON5  ** ");

  $data = SelectPos($store);

  for($i = 0; $i < count($data); $i++){
    $xml  = "<?xml version='1.0' encoding='ISO-8859-1' ?-> \n";
    $xml .= "<RESET_MOV> \n";
    $xml .= "<STORE>$store</STORE><POS>".$data[$i]["pos_number"]."</POS><TICKET>1</TICKET><TRN>1</TRN> \n";
    $xml .= "<FISCAL_STORE>$store</FISCAL_STORE><FISCAL_POS>".$data[$i]["pos_number"]."</FISCAL_POS> \n";
    $xml .= "<CASHIER_ID>0</CASHIER_ID> \n";
    $xml .= "<FISCAL_DAY>".FormatDate($date,4)."</FISCAL_DAY><FISCAL_TIME>$date</FISCAL_TIME> \n";
    $xml .= "</RESET_MOV>";
    
    $path = "/tmp/RESET_".$store."_".$data[$i]["pos_number"]."_".date('YmdHis').".xml"; 
    $arq = fopen($path, "w");
    fputs($arq, $xml); 
    fclose($arq);
  }
  unlink($file_name);
  exit;
}

// REEENVIANDO MOVIMENTO PARA CON5                            
if(substr_count ($sFile, "<RESET_MOV>") > 0 ) {
  $fiscal_date = FormatDate($fiscal_day,0);
  LogMsg("** INICIO RESET MOVIMENTO  ** ");
  LogMsg("DATA FISCAL: $fiscal_date ");
  
  LogMsg("APAGANDO TURNO DO PDV $pos DA LOJA $store");  
  $sql  = "DELETE FROM session_con5 ";
  $sql .= "WHERE 1=1 ";
  $sql .= "AND session_store_key = $store ";
  $sql .= "AND session_pos_number = $pos ";
  $sql .= "AND session_date = '$fiscal_date' ";
  $data = ConnectingInMysql($sql, 3);

  LogMsg("APAGANDO CATALOGO XML DO PDV $pos DA LOJA $store");  
  $sql  = "DELETE FROM catalog_con5 ";
  $sql .= "WHERE 1=1 ";
  $sql .= "AND catalog_store_key = $store ";
  $sql .= "AND catalog_pos_number = $pos ";
  $sql .= "AND substring(catalog_start_time,1,10) = '$fiscal_date' ";
  $data = ConnectingInMysql($sql, 3);

  LogMsg("APAGANDO TXT DO PDV $pos DA LOJA $store");
  $path = $ini["BASE_DIR"].$fiscal_day."/".str_pad($store, 4, '0',0)."/".str_pad($pos, 3, '0',0)."/";   
  $CMD = "rm -f  $path/LOJA_".$store."_PDV_".$pos."* ";
  LogMsg("COMANDO...: $CMD ");
  $rtn = exec($CMD);
  LogMsg($CMD);
  
  LogMsg("** CRIANDO FILA DE XML        ** ");
  CreateXml ( $store, $pos, $fiscal_date);                                                         
  
  LogMsg("** FIM RESET MOVIVMENTO ** ");
  unlink($file_name);
  exit;
}

// STATUS XML - CATALOGO
$statusXml = Catalog ( $store, $pos, $ticket, $date );
if($statusXml == ''){
  Catalog ( $store, $pos, $ticket, $date, 1, 0, $file_name );
}else{
  if(substr_count ($sFile, "<PAYMENT>") > 0) {
    LogMsg("XML :: ".$file_name." DO TIPO PAYMENT, NAO CONTROLADO PELA INTEGRACAO");  
  }else{
    LogMsg("XML :: ".$file_name." JA PROCESSADO EM ".$statusXml["catalog_process_time"]);
    unlink($file_name);
    exit;
  }
}

// STATUS DIRETORIO                                                                       
CreateDirectory($ini["BASE_DIR"].$fiscal_day."/".str_pad($store, 4, '0',0)."/".str_pad($pos, 3, '0',0)."/");

// XML LEITURA X
if(substr_count ($sFile, "<X_REPORT>") > 0) {
  $resultX = XReport($store, $pos, $ticket, $date, $file_name);
  if($resultX != -1){
    LogMsg("FUNCAO XREPORT FINALIZADA COM SUCESSO ::: $resultX");  
  }else{
    LogMsg("FUNCAO XREPORT NAO EXECUTADA ::: $resultX");  
  }   
}

// XML VENDA DE PRODUTOS E SERVICOS
if(substr_count ($sFile, "<SALE>") > 0 or substr_count ($sFile, "<SALE_VOID>") > 0 ) {
  $resultSale = Sale($store, $pos, $ticket, $date, $fiscal_day, $file_name);
  if($resultSale != -1){
    LogMsg("FUNCAO SALE FINALIZADA COM SUCESSO ::: $resultSale");  
  }else{
    LogMsg("FUNCAO SALE NAO EXECUTADA ::: $resultSale");  
  }   
}

// XML REDUCAO Z
if(substr_count ($sFile, "<Z_REPORT>") > 0 ) {
  $resultZ = ZReport($store, $pos, $ticket, $date, $fiscal_day, $file_name);
  if($resultZ != -1){
    LogMsg("FUNCAO ZREPORT FINALIZADA COM SUCESSO ::: $resultZ");  
  }else{
    LogMsg("FUNCAO ZREPORT NAO EXECUTADA ::: $resultZ");  
  }   
}

//RenameFile($file_name);
//Apagando arquivo XML
//unlink($file_name);

// CHAMADA DA APLICACAO DA CONECTO
$CMD = "php -q mbr_send.php $store $pos $ticket $date $file_name ";
LogMsg("\n CHAMANDO MBR_SEND :::::: ".$CMD."\n");
$rtn = exec($CMD);
LogMsg("RETORNO MBR_SEND.PHP ");
exit;


// ##########################################################
// ################                          ################
// ################  END APLICATION MAIN     ################
// ################                          ################
// ##########################################################



//*****************************************************************
//*****************  BEGIN FUNCTION XML CONDITIONS ****************
//*****************************************************************

function Sale($store, $pos, $ticket, $date, $fiscal_day, $file_name){
  
  global $ini, $aFile, $START_RECORD_ORACLE, $START_RECORD_TXT ;

  // VERIFICANDO PARAMETROS DE ENTRADA
  if(CheckParameter() === FALSE ) {
    unlink($file_name);
    return -1;
  }
  // CARREGANDO NUMERO DO ECF DO XML
  $ecf         = ReadXML($aFile, "<FISCAL_POS>");
  $fiscal_date = FormatDate($fiscal_day,0);
  LogMsg("FISCAL_DATE ::: $fiscal_date");	

  
  // CARREGANDO INFORMACOES DE VENDA 
  $sale    = SelectSale($store, $pos, $ticket, FormatDate($date,1));                               // CARREGANDO VENDA
  $items   = SelectSaleItem($store, $pos, $ticket, FormatDate($date,1));                           // CARREGANDO ITENS
  $media   = SelectSaleMediaGroup($store, $pos, $ticket, FormatDate($date,1), 'media_id, amount'); // CARREGANDO FINALIZADORAS    
  $tickets = SelectTicket($store, $pos, $ticket, FormatDate($date,1));                             // CARREGANDO TICKET

  switch ($sale["sale_type"]) {

    case 0: 
          LogMsg("::: ROTINA VENDA DE PRODUTOS - SALE_TYPE 0 :::");
          $typeName          = 'CUPOMFISCAL';
          $typeTicket        = 'CF'; 
          $typeEvent         = 'P';
          $typeTransaction   = 0;
          $resultSaleProduct = SaleProduct($sale, $items, $media, $tickets, $date, $fiscal_day, $typeName, $typeTicket, $typeEvent, $typeTransaction);
          if($resultSaleProduct != -1){
            LogMsg("FUNCAO SALEPRODUCT FINALIZADA COM SUCESSO ::: $resultSaleProduct");  
          }else{
            LogMsg("FUNCAO SALEPRODUCT NAO EXECUTADA ::: $resultSaleProduct");  
          } 
          break;
           
    case 32:
          LogMsg("::: ROTINA VENDA DE PRODUTOS - SALE_TYPE 32 :::");
          $typeName          = 'CUPOMFISCAL';
          $typeTicket        = 'CF'; 
          $typeEvent         = 'P';
          $typeTransaction   = 0;
          $resultSaleProduct = SaleProduct($sale, $items, $media, $tickets, $date, $fiscal_day, $typeName, $typeTicket, $typeEvent, $typeTransaction);
          if($resultSaleProduct != -1){
            LogMsg("FUNCAO SALEPRODUCT FINALIZADA COM SUCESSO ::: $resultSaleProduct");  
          }else{
            LogMsg("FUNCAO SALEPRODUCT NAO EXECUTADA ::: $resultSaleProduct");  
          } 
          break;

     case 410:       
          LogMsg("::: ROTINA VENDA DE SERVICOS (PAGAMENTO DE CONTAS) - SALE_TYPE 41 :::");
          $typeName          = 'CORRBAN';
          $typeTicket        = 'CB'; 
          $typeEvent         = 'C';
          $typeTransaction   = 30;
          $resultSaleService = SaleServices($sale, $media, $tickets, $date, $fiscal_day, $typeName, $typeTicket, $typeEvent, $typeTransaction);
          if($resultSaleService != -1){
            LogMsg("FUNCAO SALESERVICES FINALIZADA COM SUCESSO ::: $resultSaleService");  
          }else{
            LogMsg("FUNCAO SALESERVICES NAO EXECUTADA ::: $resultSaleService");  
          } 
          break;

    case 47:     
          LogMsg("::: ROTINA VENDA DE SERVICOS (RECARGA CELULAR) - SALE_TYPE 47 :::");
          $media   = SelectSaleMediaGroup($store, $pos, $ticket, FormatDate($date,1), 'media_id'); // CARREGANDO FINALIZADORAS    
          $typeName          = 'RECARGA';
          $typeTicket        = 'RC'; 
          $typeEvent         = 'R';
          $typeTransaction   = 1;
          $resultSaleService = SaleServices($sale, $media, $tickets, $date, $fiscal_day, $typeName, $typeTicket, $typeEvent, $typeTransaction);
          if($resultSaleService != -1){
            LogMsg("FUNCAO SALESERVICES FINALIZADA COM SUCESSO ::: $resultSaleService");  
          }else{
            LogMsg("FUNCAO SALESERVICES NAO EXECUTADA ::: $resultSaleService");  
          } 
          break;

     case 76:        //Em teste - 03-06-2016 pendente 
          LogMsg("::: ROTINA VENDA DE SERVICOS (PAGAMENTO DE TITULOS) - SALE_TYPE 76 :::");
          $typeName          = 'PAGTITULO';
          $typeTicket        = 'RC'; 
          $typeEvent         = 'R';
          $typeTransaction   = 2;
          $resultSaleService = SaleServices($sale, $media, $tickets, $date, $fiscal_day, $typeName, $typeTicket, $typeEvent, $typeTransaction);
          if($resultSaleService != -1){
            LogMsg("FUNCAO SALESERVICES FINALIZADA COM SUCESSO ::: $resultSaleService");  
          }else{
            LogMsg("FUNCAO SALESERVICES NAO EXECUTADA ::: $resultSaleService");  
          } 
          break;                                                                    

     case 80:
          LogMsg("::: ROTINA VENDA DE SERVICOS (DOACAO) - SALE_TYPE 80 :::");
          $typeName          = 'DOACAO';
          $typeTicket        = 'RC'; 
          $typeEvent         = 'R';
          $media   = SelectSaleMediaGroup($store, $pos, $ticket, FormatDate($date,1), 'media_id'); // CARREGANDO FINALIZADORAS    
          $typeTransaction = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 355);  // NRO. INSTITUICAO
          $resultSaleService = SaleServices($sale, $media, $tickets, $date, $fiscal_day, $typeName, $typeTicket, $typeEvent, $typeTransaction);
          if($resultSaleService != -1){
            LogMsg("FUNCAO SALESERVICES FINALIZADA COM SUCESSO ::: $resultSaleService");  
          }else{
            LogMsg("FUNCAO SALESERVICES NAO EXECUTADA ::: $resultSaleService");  
          } 
          break;                                                                    
     
     default:
            LogMsg("PROCESSAMENTO DEFAULT - SALE_TYPE ".$sale["sale_type"]." NAO TRATADO PELA INTEGRACAO");
            break;
  }
  return 0;
}  // END FUNCTION SALE

function SaleProduct($sale, $items, $media, $tickets, $date, $fiscal_day, $typeName, $typeTicket, $typeEvent, $typeTransaction){
  
  global $ini, $START_RECORD_ORACLE, $START_RECORD_TXT ;
  
  // RATEIO DESCONTO SUBTOTAL
  $items = ApportionDiscount ($sale, $items);          

  // CONEXAO ORACLE
  if(($link = ConnectingInOracle()) == -1){
    LogMsg("FINALIZANDO PROCESSO ::: ERRO ORACLE ");
    return -1;                                                                  // FINALIZADO COM ERRO
  }else{
    LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
  }
  if($START_RECORD_TXT === TRUE){
    // ARQUIVO POR CUPOM
    $log   = $ini["BASE_DIR"].$fiscal_day."/".str_pad($sale["store_key"], 4, '0',0)."/".str_pad($sale["pos_number"], 3, '0',0);
    $log  .= "/LOJA_".$sale["store_key"]."_PDV_".$sale["pos_number"];
    $log  .= "_".$typeName."_".$sale["ticket_number"].".txt";
    // MOVIMENTO DO PDV COMPLETO POR DATA
    $logM  = $ini["BASE_DIR"].$fiscal_day."/".str_pad($sale["store_key"], 4, '0',0)."/".str_pad($sale["pos_number"], 3, '0',0);
    $logM .= "/LOJA_".$sale["store_key"]."_PDV_".$sale["pos_number"];
    $logM .= "_DATA_".$fiscal_day.".txt";
    // ABRINDO ARQUIVOS PARA GRAVACAO
    $arqLog  = fopen($log, "w");
    $arqLogM = fopen($logM, "a");
    LogMsg("ARQUIVO          ::: ".$log);
    LogMsg("ARQUIVO COMPLETO ::: ".$logM);
  }
  // LINHA 90 - CUPOM FISCAL
  for($i = 0; $i < count($items); $i++){
    LogMsg("PROCESSANDO ITEM [".$items[$i]["desc_plu"]."] SEQUENCIA [".$items[$i]["sequence"]."]");
    // INICIANDO VARIAVEIS 
    $amountS01 = 0;
    $amountS02 = 0;
    $amountS03 = 0; 
    switch ($items[$i]["pos_id"]){
      case 'S01':
                $amountS01 = $items[$i]["amount"];
                break;
      case 'S02':
                $amountS02 = $items[$i]["amount"];
                break;
      case 'S03':
                $amountS03 = $items[$i]["amount"];
                break;
    
    }
    // ATUALIZANDO VALORES DE ISS
    if(strrpos($items[$i]["pos_id"], "S") !== false){
      Catalog ($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $date, 2, 0, 'ISS', $amountS01, $amountS02, $amountS03);
    }
    // EXTRAINDO ITEM DO ARRAY
    $lineItem = array_slice($items, $i, 1); 
    $line90   = Con5CupomFiscal($sale, $lineItem, $tickets, $date, $fiscal_day, $link, $typeName);
    if($line90 != -1){
      // ROTINA ORACLE
      if($START_RECORD_ORACLE === TRUE){
        LogMsg("::: ROTINA GRAVACAO ORACLE :::");
        $result = InsertOracle($link, 1, Mrlx_pdvimportacao($line90, 'CP'));
        LogMsg("RESULTADO DO ORACLE CP :::::: ".$result);
        $result = InsertOracle($link, 1, Mrl_logexportacao());
        LogMsg("RESULTADO DO ORACLE LOG :::::: ".$result);
	      $result = InsertOracle($link, 2);                                       //COMMIT
      }
      // ROTINA TXT
      if($START_RECORD_TXT === TRUE){
        LogMsg("::: ROTINA GRAVACAO EM ARQUIVO :::");
        // ARQUIVO POR CUPOM
        fputs($arqLog, "\n ".Mrlx_pdvimportacao($line90, 'CP').";\n"); 
        fputs($arqLog, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLog, "\n commit; \n");
        // MOVIMENTO DO PDV COMPLETO POR DATA
        fputs($arqLogM, "\n ".Mrlx_pdvimportacao($line90, 'CP').";\n"); 
        fputs($arqLogM, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLogM, "\n commit; \n");
        LogMsg("GRAVANDO REGISTRO 90 NO TXT ::::::".$line90);
      }
    }                                                                           // END IF LINE90
  }
  // LINHA 91 - PAGAMENTO E EVENTOS
  for($i = 0; $i < count($media); $i++){
    LogMsg("PROCESSANDO FINALIZADORA [".$media[$i]["media_id"]."] VALOR [".$media[$i]["amount"]."]");
    $lineMedia = array_slice($media, $i, 1);
    $line91    = Con5PagamentoEventos($sale, $lineMedia, $tickets, $date, $fiscal_day, $typeTicket, $typeEvent, $typeTransaction, $link, $typeName);
    if($line91 != -1){                
      if($START_RECORD_ORACLE === TRUE){
        LogMsg("::: ROTINA DE GRAVACAO ORACLE :::");
        $result  = InsertOracle($link, 1, Mrlx_pdvimportacao($line91, 'CP'));
        LogMsg("RESULTADO DO ORACLE CP :::::: ".$result);
        $result  = InsertOracle($link, 1, Mrl_logexportacao());
        LogMsg("RESULTADO DO ORACLE LOG :::::: ".$result);
        $result  = InsertOracle($link, 2);     //COMMIT
      }
      if($START_RECORD_TXT === TRUE){
        LogMsg("::: ROTINA DE GRAVACAO EM ARQUIVO :::");
        // ARQUIVO POR CUPOM
        fputs($arqLog, "\n ".Mrlx_pdvimportacao($line91, 'CP').";\n"); 
        fputs($arqLog, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLog, "\n commit; \n");
        // MOVIMENTO DO PDV COMPLETO POR DATA
        fputs($arqLogM, "\n ".Mrlx_pdvimportacao($line91, 'CP').";\n"); 
        fputs($arqLogM, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLogM, "\n commit; \n");
        LogMsg("GRAVANDO REGISTRO 91 NO TXT ::::::".$line91);
      }
    }                                                                           // END IF LINE91
  }
  if($START_RECORD_TXT === TRUE){
    fclose($arqLog);                                                            // FECHANDO ARQ CUPOM
    fclose($arqLogM);                                                           // FECHANDO ARQ COMPLETO
  }
  oci_close($link);                                                             // FECHANDO CONEXAO ORACLE
  return 0;                                                                     // FINALIZADO COM SUCESSO
}                                                                               // END FUNCTION SALEPRODUCT


function SaleServices($sale, $media, $tickets, $date, $fiscal_day, $typeName, $typeTicket, $typeEvent, $typeTransaction){
  
  global $ini, $START_RECORD_ORACLE, $START_RECORD_TXT, $TYPE_CARTDEB_RC, $TYPE_CARTCRED_RC  ;
  
  // CONEXAO ORACLE
  if(($link = ConnectingInOracle()) == -1){
    LogMsg("FINALIZANDO PROCESSO ::: ERRO ORACLE ");
    return -1;                                                                  // FINALIZADO COM ERRO
  }else{
    LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
  }                                        
  if($START_RECORD_TXT === TRUE){
    // ARQUIVO POR CUPOM
    $log   = $ini["BASE_DIR"].$fiscal_day."/".str_pad($sale["store_key"], 4, '0',0)."/".str_pad($sale["pos_number"], 3, '0',0);
    $log  .= "/LOJA_".$sale["store_key"]."_PDV_".$sale["pos_number"];
    $log  .= "_".$typeName."_".$sale["ticket_number"].".txt";
    // MOVIMENTO DO PDV COMPLETO POR DATA
    $logM  = $ini["BASE_DIR"].$fiscal_day."/".str_pad($sale["store_key"], 4, '0',0)."/".str_pad($sale["pos_number"], 3, '0',0);
    $logM .= "/LOJA_".$sale["store_key"]."_PDV_".$sale["pos_number"];
    $logM .= "_DATA_".$fiscal_day.".txt";
    // ABRINDO ARQUIVOS PARA GRAVACAO
    $arqLog  = fopen($log, "w");
    $arqLogM = fopen($logM, "a");
    LogMsg("ARQUIVO          ::: ".$log);
    LogMsg("ARQUIVO COMPLETO ::: ".$logM);
  }
    
  // LINHA 91 - PAGAMENTO E EVENTOS
  for($i = 0; $i < count($media); $i++){
    LogMsg("PROCESSANDO FINALIZADORA [".$media[$i]["media_id"]."] VALOR [".$media[$i]["amount"]."]");
    $lineMedia = array_slice($media, $i, 1);
    $line91    = Con5PagamentoEventos($sale, $lineMedia, $tickets, $date, $fiscal_day, $typeTicket, $typeEvent, $typeTransaction, $link, $typeName);
    // TRATAMENTO DO CONTRA PARTIDA DA RECARGA PAGA EM CART.DEBITO
    if($media[$i]["media_id"] == 5 and $typeName == 'RECARGA'){
      $line91    = Con5PagamentoEventos($sale, $lineMedia, $tickets, $date, $fiscal_day, $typeTicket, $typeEvent, $TYPE_CARTDEB_RC, $link, $typeName);
      $typeTicket= 'CF'; 
      $typeEvent = 'P';
      $line91_RC = Con5PagamentoEventos($sale, $lineMedia, $tickets, $date, $fiscal_day, $typeTicket, $typeEvent, $typeTransaction, $link, $typeName);
    }
    // TRATAMENTO DO CONTRA PARTIDA DA RECARGA PAGA EM CART.CREDITO    
    if($media[$i]["media_id"] == 3 and $typeName == 'RECARGA'){
      $line91    = Con5PagamentoEventos($sale, $lineMedia, $tickets, $date, $fiscal_day, $typeTicket, $typeEvent, $TYPE_CARTCRED_RC, $link, $typeName);
      $typeTicket= 'CF'; 
      $typeEvent = 'P';
      $line91_RC = Con5PagamentoEventos($sale, $lineMedia, $tickets, $date, $fiscal_day, $typeTicket, $typeEvent, $typeTransaction, $link, $typeName);
    }
    if($line91 != -1){
      if($START_RECORD_ORACLE === TRUE){
        LogMsg("::: ROTINA DE GRAVACAO ORACLE :::");
        $result  = InsertOracle($link, 1, Mrlx_pdvimportacao($line91, 'CP'));
        LogMsg("RESULTADO DO ORACLE CP :::::: ".$result);
        $result  = InsertOracle($link, 1, Mrl_logexportacao());
        LogMsg("RESULTADO DO ORACLE LOG :::::: ".$result);
        $result  = InsertOracle($link, 2);     //COMMIT
        // CONTRA PARTIDA RECARGA PAGA EM CART.DEBITO
        if($media[$i]["media_id"] == 5 and $typeName == 'RECARGA'){
          LogMsg("::: CONTRA PARTIDA RECARGA PAGA CART.DEBITO :::");
          $result  = InsertOracle($link, 1, Mrlx_pdvimportacao($line91_RC, 'CP'));
          LogMsg("RESULTADO DO ORACLE CP :::::: ".$result);
          $result  = InsertOracle($link, 1, Mrl_logexportacao());
          LogMsg("RESULTADO DO ORACLE LOG :::::: ".$result);
          $result  = InsertOracle($link, 2);     //COMMIT
        }
        // CONTRA PARTIDA RECARGA PAGA EM CART.CREDITO
        if($media[$i]["media_id"] == 3 and $typeName == 'RECARGA'){
          LogMsg("::: CONTRA PARTIDA RECARGA PAGA CART.CREDITO :::");
          $result  = InsertOracle($link, 1, Mrlx_pdvimportacao($line91_RC, 'CP'));
          LogMsg("RESULTADO DO ORACLE CP :::::: ".$result);
          $result  = InsertOracle($link, 1, Mrl_logexportacao());
          LogMsg("RESULTADO DO ORACLE LOG :::::: ".$result);
          $result  = InsertOracle($link, 2);     //COMMIT
        }
      }
      if($START_RECORD_TXT === TRUE){
        LogMsg("::: ROTINA DE GRAVACAO EM ARQUIVO :::");
        // ARQUIVO POR CUPOM
        fputs($arqLog, "\n ".Mrlx_pdvimportacao($line91, 'CP').";\n"); 
        fputs($arqLog, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLog, "\n commit; \n");
        // MOVIMENTO DO PDV COMPLETO POR DATA
        fputs($arqLogM, "\n ".Mrlx_pdvimportacao($line91, 'CP').";\n"); 
        fputs($arqLogM, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLogM, "\n commit; \n");
        // CONTRA PARTIDA RECARGA PAGA EM CART.DEBITO
        if($media[$i]["media_id"] == 5 and $typeName == 'RECARGA'){        
          // ARQUIVO POR CUPOM
          fputs($arqLog, "\n ".Mrlx_pdvimportacao($line91_RC, 'CP').";\n"); 
          fputs($arqLog, "\n ".Mrl_logexportacao().";\n");
          fputs($arqLog, "\n commit; \n");
          // MOVIMENTO DO PDV COMPLETO POR DATA
          fputs($arqLogM, "\n ".Mrlx_pdvimportacao($line91_RC, 'CP').";\n"); 
          fputs($arqLogM, "\n ".Mrl_logexportacao().";\n");
          fputs($arqLogM, "\n commit; \n");
        }
        LogMsg("GRAVANDO REGISTRO 91 NO TXT ::::::".$line91);
      }
    }                                                                           // END IF LINE91
  }
  if($START_RECORD_TXT === TRUE){
    fclose($arqLog);                                                            // FECHANDO ARQ CUPOM
    fclose($arqLogM);                                                           // FECHANDO ARQ COMPLETO
  }
  oci_close($link);                                                             // FECHANDO CONEXAO ORACLE
  return 0;                                                                     // FINALIZADO COM SUCESSO
}                                                                               // END FUNCTION SALEPRODUCT


function XReport($store, $pos, $ticket, $date, $file_name){
  
  global $ini, $aFile, $START_RECORD_ORACLE, $START_RECORD_TXT, $START_NOTIFICATION, $START_SEND_EMAIL  ;
  
  // VERIFICANDO PARAMETROS DE ENTRADA
  if(CheckParameter() === FALSE ) {
    unlink($file_name);
    return -1;
  }

  // CARREGANDO INFORMACOES DA LEITURA X 
  $cashier        = ReadXML($aFile, "<CASHIER_ID>");
  $initialGT      = ReadXML($aFile, "<INITIAL_GT>");
  $endGT          = ReadXML($aFile, "<FINAL_GT>");
  $amountVoid     = ReadXML($aFile, "<AMOUNT_VOID>");
  $amountDiscount = ReadXML($aFile, "<AMOUNT_DISCOUNT>");
  $notification   = SelectNotification($store, $pos, $ticket, 71);
  $tickets        = SelectTicket($store, $pos, $ticket, FormatDate($date,1));

  LogMsg("Initial GT.....:".$initialGT);
  LogMsg("End GT.........:".$endGT);
  LogMsg("Status.........:".$notification["status"]);
  LogMsg("String.........:".$notification["notification_data"]);
  LogMsg("Amount Void....:".$amountVoid);
  LogMsg("Amount Discount:".$amountDiscount);
  LogMsg("Cashier........:".$cashier);
  LogMsg("Data Fiscal....:".$tickets["fiscal_date"]);

  switch ($notification["status"]) {

    case 'Entrada':
      LogMsg("ENTRADA DE OPERADOR ");
      $sessionPos = Con5Session($store, $pos, FormatDate($date,0), $ticket, $cashier, 0 );
      $type = 1;
      if($sessionPos == '') {
        Con5Session($store, $pos, FormatDate($date,0), $ticket, $cashier, $type, $endGT, $amountVoid, $amountDiscount, 1);
      }else{
        Con5Session($store, $pos, FormatDate($date,0), $ticket, $cashier, $type, $endGT, $amountVoid, $amountDiscount, ($sessionPos["session_number"]+1));        
      }
      $line91 = Con5Eventos($store, $pos, $ticket, $date, $cashier, $endGT, 'EV', 'A', $amountVoid, $tickets);
       
      //GRAVANDO ORACLE
      if($START_RECORD_ORACLE === TRUE){ 
        if(($link = ConnectingInOracle()) == -1){
          LogMsg("FINALIZANDO PROCESSO ::: ERRO ORACLE ");
          return -1;
        }else{
          LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
          //EVENTO ABERTURA
          $result = InsertOracle($link, 1, Mrlx_pdvimportacao($line91, 'CP'));
          LogMsg("Resultado do Oracle EV :::::: ".$result);
          $result = InsertOracle($link, 1, Mrl_logexportacao());
          LogMsg("Resultado do Oracle Log :::::: ".$result);
	  //COMMIT
          $result = InsertOracle($link, 2);
        }
        oci_close($link);
      }
      //GRAVANDO EM ARQUIVO
      if($START_RECORD_TXT === TRUE){
        LogMsg("::: ROTINA DE GRAVACAO EM ARQUIVO :::");
        $dateMov = str_replace('-', '', $tickets["fiscal_date"]);  // Data YYYMMDD 
        $log = $ini["BASE_DIR"].FormatDate($date,4)."/".str_pad($store, 4, '0',0)."/".str_pad($pos, 3, '0',0)."/LOJA_".$store."_PDV_".$pos."_ENTRADAOPER_".$ticket.".txt";
        $arqLog = fopen($log, "w"); 
        //Movimento do pdv completo por data 
        $logM = $ini["BASE_DIR"].FormatDate($date,4)."/".str_pad($store, 4, '0',0)."/".str_pad($pos, 3, '0',0)."/LOJA_".$store."_PDV_".$pos."_DATA_".$dateMov.".txt"; 
        $arqLogM = fopen($logM, "a"); 
        fputs($arqLog, "\n ".Mrlx_pdvimportacao($line91, 'CP').";\n"); 
        fputs($arqLog, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLog, "\n commit; \n");
        //Movimento do pdv completo por data 
        fputs($arqLogM, "\n ".Mrlx_pdvimportacao($line91, 'CP').";\n"); 
        fputs($arqLogM, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLogM, "\n commit; \n");
        LogMsg("ARQUIVO           ::: ".$log);
        LogMsg("ARQUIVO  COMPLETO ::: ".$logM);
        LogMsg("GRAVANDO REGISTRO 91 NO TXT ::::::".$line91);
      }
      break;

    case 'Saida':
      LogMsg("SAIDA DE OPERADOR ");
      $sessionPos = Con5Session($store, $pos, $tickets["fiscal_date"], $ticket, $cashier, 4 );
      if($sessionPos == ''){
        $msg = "LOJA : $store  PDV : $pos  COO : $ticket - ABERTURA NAO ENCONTRADA PARA O OPERADOR...: $cashier "; 
        LogMsg($msg);
        // TRATAMENTO DE ALERTAS
        if($START_NOTIFICATION === TRUE){
          $result = InsertNotification($store, $pos, $ticket, 15, 102, $msg);
        }      
        if($START_SEND_EMAIL === TRUE){
          $send = SendEmail("jarison@remaqbh.com.br", "emporium@drogariaminasbrasil.com.br", "Alertas Integracao MBR", $msg, "kerley@remaqbh.com.br");
        }    
        //Apagando arquivo XML
        unlink($file_name);
        return -1;      
      }else{
        Con5Session($store, $pos, $tickets["fiscal_date"], $sessionPos["session_ticket_number"], $cashier, 3, $endGT, $amountVoid, $amountDiscount, $sessionPos["session_ticket_number"], $ticket);
      } 
      $line91 = Con5Eventos($store, $pos, $ticket, $date, $cashier, $endGT, 'EV', 'F', $amountVoid, $tickets);
      
      //GRAVANDO ORACLE
      if($START_RECORD_ORACLE === TRUE){
        if(($link = ConnectingInOracle()) == -1){
          LogMsg("FINALIZANDO PROCESSO ::: ERRO ORACLE ");
          return -1;
        }else{
          LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
          //EVENTO FECHAMENTO
          $result = InsertOracle($link, 1, Mrlx_pdvimportacao($line91, 'CP'));
          LogMsg("Resultado do Oracle EV :::::: ".$result);
          $result = InsertOracle($link, 1, Mrl_logexportacao());
          LogMsg("Resultado do Oracle Log :::::: ".$result);  //COMMIT
          $result = InsertOracle($link, 2);
        }
        oci_close($link);
      }
      //GRAVANDO EM ARQUIVO
      if($START_RECORD_TXT === TRUE){
        LogMsg("::: ROTINA DE GRAVACAO EM ARQUIVO :::");
        $dateMov = str_replace('-', '', $tickets["fiscal_date"]);  // Data YYYMMDD 
        $log = $ini["BASE_DIR"].$dateMov."/".str_pad($store, 4, '0',0)."/".str_pad($pos, 3, '0',0)."/LOJA_".$store."_PDV_".$pos."_SAIDAOPER_".$ticket.".txt";
        $arqLog = fopen($log, "w"); 
        //Movimento do pdv completo por data 
        $logM = $ini["BASE_DIR"].$dateMov."/".str_pad($store, 4, '0',0)."/".str_pad($pos, 3, '0',0)."/LOJA_".$store."_PDV_".$pos."_DATA_".$dateMov.".txt";
        $arqLogM = fopen($logM, "a");
        fputs($arqLog, "\n ".Mrlx_pdvimportacao($line91, 'CP').";\n"); 
        fputs($arqLog, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLog, "\n commit; \n");
        //Movimento do pdv completo por data 
        fputs($arqLogM, "\n ".Mrlx_pdvimportacao($line91, 'CP').";\n"); 
        fputs($arqLogM, "\n ".Mrl_logexportacao().";\n");
        fputs($arqLogM, "\n commit; \n");
        LogMsg("ARQUIVO           ::: ".$log);
        LogMsg("ARQUIVO  COMPLETO ::: ".$logM);
        LogMsg("GRAVANDO REGISTRO 91 NO TXT ::::::".$line91);
      }
      break;

    default:
      LogMsg("ACAO DEFAULT ");
      break;
  }
  fclose($arqLog); //Apagar
  fclose($arqLogM); //Apagar
  return 0;
}   // END FUNCTION XREPORT

function ZReport($store, $pos, $ticket, $date, $fiscal_day, $file_name){
   
  global $ini, $aFile, $START_RECORD_ORACLE, $START_RECORD_TXT ;

  LogMsg("PROCESSAMENTO REDUCAO Z...");
  
  // VERIFICANDO PARAMETROS DE ENTRADA  
  if(CheckParameter() === FALSE ) {
    unlink($file_name);
    return -1;
  }
  
  // CARREGANDO ECF_NUMBER DO XML
  $ecf         = ReadXML($aFile, "<FISCAL_POS>");
  $fiscal_date = FormatDate($fiscal_day,0);
  LogMsg("FISCAL_DATE ::: $fiscal_date");	

  // CARREGANDO DADOS DA Z 
  $fiscal        = SelectFiscalStatus($store, $pos, $ticket, FormatDate($date,1));                        // CARREGANDO REDUCAO Z
  $fiscalTax     = SelectFiscalTaxStatus($store, $pos, $ticket, FormatDate($date,1));                     // CARREGANDO IMPOSTOS
  $tabTicket     = SelectTicket($store, $pos, $ticket, FormatDate($date,1));                              // CARREGANDO TICKET
  $itemTaxes     = SelectFiscalAccumItem($store, $pos, $fiscal_date);                                     // CARREGANDO IMPOSTOS ITENS
  $serialEcf     = SerialNumberECF($tabTicket["store_key"],$tabTicket["ecf_number"]);                     // NUMERO SERIE DA ECF
  $amountVoidIss = '0.00';                                                                                // VALOR CANCELAMENTO ISS
  $amountDiscIss = '0.00';                                                                                // VALOR DESCONTO ISS
  $amountSaleIss = '0.00';                                                                                // VALOR VENDA ISS
  
  // CALCULANDO VALORES DE ISS
  for($i = 0; $i < count($itemTaxes); $i++){
    if(substr($itemTaxes[$i]["pos_id"],0,1) == 'S'){
      $amountVoidIss += $itemTaxes[$i]["amount_canc"];                                                 
      $amountSaleIss += $itemTaxes[$i]["amount"];                                                 
    }    
  }
  // CONEXAO ORACLE
  if(($link = ConnectingInOracle()) == -1){
    LogMsg("FINALIZANDO PROCESSO ::: ERRO ORACLE ");
    return -1;                                                                                            // FINALIZADO COM ERRO
  }else{
    LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
  }
  // ARQUIVO TXT
  if($START_RECORD_TXT === TRUE){ 
    // ARQUIVO POR CUPOM
    $log   = $ini["BASE_DIR"].$fiscal_day."/".str_pad($fiscal["store_key"], 4, '0',0)."/".str_pad($fiscal["pos_number"], 3, '0',0);
    $log  .= "/LOJA_".$fiscal["store_key"]."_PDV_".$fiscal["pos_number"];
    $log  .= "_ZREPORT_".$fiscal["ticket_number"].".txt";
    // MOVIMENTO DO PDV COMPLETO POR DATA
    $logM  = $ini["BASE_DIR"].$fiscal_day."/".str_pad($fiscal["store_key"], 4, '0',0)."/".str_pad($fiscal["pos_number"], 3, '0',0);
    $logM .= "/LOJA_".$fiscal["store_key"]."_PDV_".$fiscal["pos_number"];
    $logM .= "_DATA_".$fiscal_day.".txt";
    // ABRINDO ARQUIVOS PARA GRAVACAO
    $arqLog  = fopen($log, "w");
    $arqLogM = fopen($logM, "a");
    LogMsg("ARQUIVO          ::: ".$log);
    LogMsg("ARQUIVO COMPLETO ::: ".$logM);
  }
  // REDUCAO Z
  $line92 = Con5ReducaoZ($date, $fiscal_day, $fiscal, $serialEcf, $amountVoidIss, $amountDiscIss);        // LINHA 92 - REDUCAO Z
  // GRAVACAO
  if($START_RECORD_ORACLE === TRUE){
    LogMsg("::: ROTINA GRAVACAO ORACLE :::");
    $result = InsertOracle($link, 1, Mrlx_pdvimportacao($line92, 'RZ'));                                  // GRAVANDO DADOS POR CUPOM
    LogMsg("RESULTADO DO ORACLE Z :::::: $result");                                                       // RESULTADO ORACLE
  }
  if($START_RECORD_TXT === TRUE){
    LogMsg("::: ROTINA GRAVACAO EM ARQUIVO :::");
    fputs($arqLog, "\n ".Mrlx_pdvimportacao($line92, 'RZ').";\n");                                        // GRAVANDO ARQUIVO POR CUPOM 
    fputs($arqLogM, "\n ".Mrlx_pdvimportacao($line92, 'RZ').";\n");                                       // GRAVANDO MOVIMENTO DO PDV COMPLETO POR DATA 
    LogMsg("GRAVANDO REGISTRO 92 NO TXT ::::::".$line92);
  }
  //ALIQUOTAS
  for($i = 0; $i < count($fiscalTax); $i++){
    LogMsg("PROCESSANDO ALIQUOTA [".$fiscalTax[$i]["pos_id"]."]  [".$fiscalTax[$i]["tax_percent"]."] % ");  
    $lineTax = array_slice($fiscalTax, $i, 1); 
    $line93 = Con5AliquotasZ($fiscal_day, $lineTax, $serialEcf, $link);                                   // LINHA 93 - ALIQUOTAS DA REDUCAO Z
    if($START_RECORD_ORACLE === TRUE){
      LogMsg("::: ROTINA GRAVACAO ORACLE :::");
      $result = InsertOracle($link, 1, Mrlx_pdvimportacao($line93, 'RZ'));                                // GRAVANDO DADOS POR CUPOM
      LogMsg("RESULTADO DO ORACLE TR :::::: $result");                                                    // RESULTADO ORACLE
    }
    if($START_RECORD_TXT === TRUE){
      LogMsg("::: ROTINA GRAVACAO EM ARQUIVO :::");
      fputs($arqLog, "\n ".Mrlx_pdvimportacao($line93, 'RZ').";\n");                                      // GRAVANDO ARQUIVO POR CUPOM 
      fputs($arqLogM, "\n ".Mrlx_pdvimportacao($line93, 'RZ').";\n");                                     // GRAVANDO MOVIMENTO DO PDV COMPLETO POR DATA
      LogMsg("GRAVANDO REGISTRO 93 NO TXT ::::::".$line93);
    }
  }
  // VALORES DE ISS DA REDUCAO Z
  $sql  = "UPDATE CONSINCO.MFL_REDUCAOZ SET ";
  $sql .= "VLRTOTALCANCISS  = '$amountVoidIss', ";
  $sql .= "VLRTOTALDESCISS  = '$amountDiscIss', ";
  $sql .= "VLRTOTALVENDAISS = '$amountSaleIss' ";
  $sql .= "WHERE 1=1 ";
  $sql .= "AND DTAMOVIMENTO = '".FormatDate($fiscal_day,7)."' ";
  $sql .= "AND NROSERIEECF  = '".$serialEcf["ecf_serial"]."' ";
  $sql .= "AND NROEMPRESA   = $store ";
  // GRAVACAO
  if($START_RECORD_ORACLE === TRUE){
    LogMsg("::: ROTINA GRAVACAO ORACLE :::");
    $result = InsertOracle($link, 1, Mrl_logexportacao());
    LogMsg("RESULTADO DO ORACLE LOG :::::: $result ");
    $result = InsertOracle($link, 2);                                                                     // COMMIT ORACLE
    // ATUALIZANDO REDUCAO Z COM VALORES DE ISS
    LogMsg(" ATUALIZANDO BASE CON5 ::: ");
    $result = InsertOracle($link, 1, $sql);                                                               // GRAVANDO DADOS ORACLE  
    $result = InsertOracle($link, 2);                                                                     // COMMIT ORACLE
    LogMsg("RESULTADO DO ORACLE :::::: $result");                                                         // RESULTADO ORACLE
  }
  if($START_RECORD_TXT === TRUE){
    LogMsg("::: ROTINA GRAVACAO EM ARQUIVO :::");
    // ARQUIVO POR CUPOM 
    fputs($arqLog, "\n ".Mrl_logexportacao().";\n");
    fputs($arqLog, "\n commit; \n");
    fputs($arqLog, "\n $sql ; \n");
    fputs($arqLog, "\n commit; \n");
    fclose($arqLog); 
    // MOVIMENTO DO PDV COMPLETO POR DATA
    fputs($arqLogM, "\n ".Mrl_logexportacao().";\n");
    fputs($arqLogM, "\n commit; \n");
    fputs($arqLogM, "\n $sql ; \n");
    fputs($arqLogM, "\n commit; \n");
    fclose($arqLogM); 
  }
  oci_close($link);                                                                                       // FECHANDO CONEXAO ORACLE
  return 0;
} // END FUNCTION ZREPORT


//*****************************************************************
//*****************  END FUNCTION XML CONDITIONS ****************
//*****************************************************************


//*****************************************************************
//***************  BEGIN FUNCTION INTEGRATION CON5  ***************
//*****************************************************************

function Con5Session($store, $pos, $dateInc, $ticket = 0, $cashierNumber = 0, $type = 0, $endGT = 0, $amountVoid = 0, $amountDiscount = 0, $sessionNumber = 1, $endTicket = 0){
  
  switch ($type) {
    case 0:
      $sql  = "SELECT * FROM session_con5 ";
      $sql .= " WHERE session_store_key = ".$store;
      $sql .= " AND session_pos_number = ".$pos;
      $sql .= " AND session_date = '".$dateInc."'";
      $sql .= " ORDER BY session_date, session_number DESC ";
      $sql .= " LIMIT 1";
      LogMsg("SQL table session_con5 ::".$sql);
      $data = ConnectingInMysql($sql, 2);
      if(count($data) == 0) $data = ''; 
      break;
    case 1:
      $sql  = "INSERT INTO session_con5 ";
      $sql .= " ( ";
      $sql .= " session_store_key, ";
      $sql .= " session_pos_number, ";
      $sql .= " session_date, ";
      $sql .= " session_number, ";
      $sql .= " session_cashier, ";
      $sql .= " session_cashier_gt, ";
      $sql .= " session_cashier_void,";
      $sql .= " session_cashier_discount, ";
      $sql .= " session_ticket_number ";      
      $sql .= " ) ";
      $sql .= " VALUES ( ";
      $sql .= $store.", ";
      $sql .= $pos.", ";
      $sql .= "'".$dateInc."', ";
      $sql .= $sessionNumber.", ";
      $sql .= $cashierNumber.", ";
      $sql .= $endGT.", ";
      $sql .= $amountVoid.", ";
      $sql .= $amountDiscount.", ";
      $sql .= $ticket;
      $sql .= " ) ";
      $data = ConnectingInMysql($sql, 3);
      break;
    case 2:
      $sql  = "UPDATE session_con5 ";
      $sql .= " SET session_store_key = ".$store.", session_pos_number = ".$pos.", ";
      $sql .= " session_date = '".$dateInc."', session_number = session_number + 1, session_cashier = ".$cashierNumber.", ";
      $sql .= " session_cashier_gt = '".$endGT."', session_cashier_void = ".$amountVoid.", session_cashier_discount = ".$amountDiscount;
      $sql .= " WHERE session_store_key = ".$store;
      $sql .= " AND session_pos_number = ".$pos;
      $sql .= " AND session_date = '".$dateInc."'";
      $data = ConnectingInMysql($sql, 3);
      break;
    case 3:
      $sql  = "UPDATE session_con5 ";
      $sql .= " SET session_store_key = ".$store.", session_pos_number = ".$pos.", ";
      $sql .= " session_date = '".$dateInc."', session_cashier = ".$cashierNumber.", ";
      $sql .= " session_cashier_gt = '".$endGT."', session_cashier_void = '".$amountVoid."', ";
      $sql .= " session_cashier_discount = '".$amountDiscount."', session_ticket_end = ".$endTicket;
      $sql .= " WHERE session_store_key = ".$store;
      $sql .= " AND session_pos_number = ".$pos;
      $sql .= " AND session_ticket_number = ".$ticket;
      $sql .= " AND session_date = '".$dateInc."'";
      $data = ConnectingInMysql($sql, 3);
      break;
    case 4:
      $sql  = "SELECT * FROM session_con5 ";
      $sql .= " WHERE session_store_key = ".$store;
      $sql .= " AND session_pos_number = ".$pos;
      //$sql .= " AND session_ticket_number = ".$ticket;   
      $sql .= " AND session_cashier = ".$cashierNumber;      
      $sql .= " AND session_date = '".$dateInc."'";
      $sql .= " ORDER BY session_date, session_number DESC ";
      $sql .= " LIMIT 1";
      LogMsg("SQL table session_con5 ::".$sql);
      $data = ConnectingInMysql($sql, 2);
      if(count($data) == 0) $data = ''; 
      break;
    case 5:
      $sql  = "UPDATE session_con5 ";
      $sql .= " SET session_store_key = ".$store.", session_pos_number = ".$pos.", ";
      $sql .= " session_date = '".$dateInc."', session_number = ".$sessionNumber.", session_cashier = ".$cashierNumber.", ";
      $sql .= " session_cashier_gt = '".$endGT."', session_cashier_void = ".$amountVoid.", session_cashier_discount = ".$amountDiscount;
      $sql .= " WHERE session_store_key = ".$store;
      $sql .= " AND session_pos_number = ".$pos;
      $sql .= " AND session_date = '".$dateInc."'";
      $data = ConnectingInMysql($sql, 3);
      break;
    default:
      $sql  = "SELECT * FROM session_con5 ";
      $sql .= " WHERE session_store_key = ".$store;
      $sql .= " AND session_pos_number = ".$pos;
      $sql .= " AND session_date = '".$dateInc."'";
      $sql .= " ORDER BY session_date, session_number DESC ";
      $sql .= " LIMIT 1";
      $data = ConnectingInMysql($sql, 2);
      if(count($data) == 0) $data = ''; 
      break;
  } 
  LogMsg("SESSION CON5 ::: ".$sql);
  return $data;
}

function Con5CupomFiscal($sale, $item, $ticket, $date, $fiscal_day, $mainLink = -1, $typeName = 'Cupom Fiscal'){

  global $START_NOTIFICATION, $START_SEND_EMAIL  ;

  $cli       = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 1);                           // CLIENTE 
  $cnpjCpf   = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 3);                           // CPF/CNPJ
  $seqLot    = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 352);                         // LOTE   
  $itemCanc  = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 162, $item[0]["sequence"]);   // ITEM CANCELADO
  $request   = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 6,FALSE, $item[0]["sequence"], " ORDER BY item_sequence DESC LIMIT 1 ");                                                                                                                                // NRO. PEDIDO  
  $vendedor  = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 20,FALSE, $item[0]["sequence"], " ORDER BY item_sequence DESC LIMIT 1 " );                                                                                                                             // VENDEDOR  
  $cliente   = ($cli == '' ? $cnpjCpf : $cli );                                                                                                      // COD. CLIENTE
  $cliente   = '';  // Fazer teste - apresentando erro de insercao 
  $cgo       = 460;                                                                                                                                  
  $history   = "$typeName: [".$sale["ticket_number"]."]   ORCAMENTO: [$request]   PDV: [".$sale["pos_number"]."]   SOFT:[EMPORIUM]";                 // FIELD 38
     
  // VERIFICANDO ITEM CANCELADO  
  if($itemCanc != $item[0]["sequence"]){
    
    if($mainLink == -1){
      if(($link = ConnectingInOracle()) == -1){
        LogMsg("FINALIZANDO PROCESSO ::: ERRO CONEXAO ORACLE ");
        return -1;                                                              
      }
      LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
    }else{
      LogMsg("USANDO LINK PRINCIPAL DO ORACLE ");
      $link = $mainLink; 
    }
    // PESQUISANDO COD VENDEDOR 
    if($vendedor != ''){
      $msg = "LOJA : ".$sale["store_key"]."  PDV : ".$sale["pos_number"]."  COO : ".$sale["ticket_number"]." - CUPOM COM VENDEDOR :: PESQUISANDO VENDEDOR $vendedor NO EMPORIUM "; 
      LogMsg($msg);
      $clerk = SelectMysqlClerk($vendedor);
      if($clerk["agent_key"] == ''){
        $vendedor = '92';
        $msgError = "USANDO DADOS DEFAULT VENDEDOR $vendedor  MOTIVO :: NAO LOCALIZADO NO EMPORIUM.";
        LogMsg($msgError);
        // TRATAMENTO DE ALERTAS
        if($START_NOTIFICATION === TRUE){
          $result = InsertNotification($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], 15, 102, $msg.$msgError);
        }      
        if($START_SEND_EMAIL === TRUE){
          $send = SendEmail("jarison@remaqbh.com.br", "emporium@drogariaminasbrasil.com.br", "Alertas Integracao MBR", $msg.$msgError, "kerley@remaqbh.com.br");
        }    
      }else{
        LogMsg("VENDEDOR $vendedor LOCALIZADO COM SUCESSO NO EMPORIUM.");
      }
    }
    // PESQUISANDO COD OPERADOR CON5
    $oper = SelectMysqlOper($sale["cashier_key"]);
    $sql  = "SELECT codopefrentepdv, seqoperador ";
    $sql .= "FROM CONSINCO.mfl_operador ";
    $sql .= "WHERE 1=1 ";
    $sql .= "AND nroempresa = ".$sale["store_key"]." ";
    $sql .= "AND codopefrentepdv = ".$oper["alternate_id"]." ";
    $sql .= "AND statusoperador = 'A' ";
    $result = SelectOracle($link, $sql, 2);
    $seqOperCon5 = $result["SEQOPERADOR"];
    LogMsg("PESQUISANDO BASE CONSINCO...: ".$sql);
    // FECHANDO CONEXAO ORACLE - MAIN
    if($mainLink == -1) oci_close($link);                                       // FECHANDO LINK SEGUNDARIO ORACLE
    LogMsg("Dados da tabela mfl_operador ...: OPERADOR EMPORIUM ::: ".$result["CODOPEFRENTEPDV"]." --- CONSINCO ::: ".$seqOperCon5);
    if($seqOperCon5 == ''){
      $seqOperCon5 = 92;
      $msgError  = "LOJA : ".$sale["store_key"]."  PDV : ".$sale["pos_number"]."  COO : ".$sale["ticket_number"]." :: ";
      $msgError .= "OPERADOR ".$oper["alternate_id"]." :: USANDO DADOS DEFAULT  :: ".$seqOperCon5." MOTIVO :: PROBLEMA NO CADASTRO OPERADOR/LOJA NA CON5.";
      LogMsg($msgError);
      // TRATAMENTO DE ALERTAS
      if($START_NOTIFICATION === TRUE){
        $result = InsertNotification($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], 15, 102, $msgError);
      }      
      //if($START_SEND_EMAIL === TRUE){
      //  $send = SendEmail("jarison@remaqbh.com.br", "emporium@drogariaminasbrasil.com.br", "Alertas Integracao MBR", $msgError, "kerley@remaqbh.com.br");
      //}
    }
    // SERIAL DA ECF
    $serialEcf = SerialNumberECF($ticket["store_key"],$ticket["ecf_number"]);	                                           // NRO.SERIE ECF
    // TRATAMENTO DOS CANCELAMENTOS  GARCIA
    if($sale["voided"] == 1 or $sale["post_sale_void"] == 1) $statusCupom='C';else $statusCupom='V';                     // FIELD 10  
    if($sale["post_sale_void"] == 1) $numCupom = $sale["void_ticket_number"];else $numCupom = $sale["ticket_number"];    // FIELD 10  
    if($item[0]["voided"] == 1 or $statusCupom == 'C') $statusItem = 'C'; else $statusItem = 'V';                        // STATUS DO ITEM - C->CANCELADO, V->VALIDO 
    // TRATAMENTO DOS CANCELAMENTOS MINAS BRASIL
    $statusCupom  = 'V';
    $statusItem   = 'V';
    $numCupom     = $sale["ticket_number"];
    $itemDiscount = $item[0]["discount"];
    $voidDiscount = "0.00";   // SOLICITADO PELO WILLIAM 17/02  - EM ANALISE  
    if($item[0]["voided"] == 1){
      $statusItem   = 'C';
      $itemDiscount = "0.00";
    }
    if($sale["voided"] == 1){
      $statusItem   = 'C';
      $itemDiscount = "0.00";
    }
    if($sale["post_sale_void"] == 1) {
      $statusCupom  = 'C';
      $statusItem   = 'C';
      $numCupom     = $sale["void_ticket_number"];
      $itemDiscount = "0.00";
      //$voidDiscount = $item[0]["discount"];
    }
    // NOVO ALGORITMO  MINAS BRASIL
    switch ($item[0]["pos_id"]) {
    
      case 'F':
              $sumAmountTaxI = '0.00';                                                                                         // FIELD 22
              $amountOther   = $item[0]["amount"] + $voidDiscount;                                                             // FIELD 23 
              $baseCalICMS   = '0.00';                                                                                         // FIELD 24
              $amountIcms    = '0.00';                                                                                         // FIELD 32  
              $amountIcmsST  = '0.00';                                                                                         // FIELD 33 
              $baseCalIcmsST = '0.00';                                                                                         // FIELD 39
              $cfop          = 5405;                                                                                           // FIELD 44      
              $seqTrib       = $item[0]["pos_id"]."1";                                                                         // FIELD 71
              break;

      case 'I':
              $sumAmountTaxI = $item[0]["amount"] + $voidDiscount;                                                             // FIELD 22
              $amountOther   = '0.00';                                                                                         // FIELD 23  
              $baseCalICMS   = '0.00';                                                                                         // FIELD 24
              $amountIcms    = '0.00';                                                                                         // FIELD 32  
              $amountIcmsST  = '0.00';                                                                                         // FIELD 33 
              $baseCalIcmsST = '0.00';                                                                                         // FIELD 39
              $cfop          = 5102;                                                                                           // FIELD 44      
              $seqTrib       = $item[0]["pos_id"]."1";                                                                         // FIELD 71 
              break;
    
      case 'N':
              $sumAmountTaxI = '0.00';                                                                                         // FIELD 22
              $amountOther   = '0.00';                                                                                         // FIELD 23  
              $baseCalICMS   = '0.00';                                                                                         // FIELD 24
              $amountIcms    = '0.00';                                                                                         // FIELD 32  
              $amountIcmsST  = '0.00';                                                                                         // FIELD 33 
              $baseCalIcmsST = '0.00';                                                                                         // FIELD 39
              $cfop          = 0000;                                                                                           // FIELD 44      
              $seqTrib       = $item[0]["pos_id"]."1";                                                                         // FIELD 71 
              break;

      default:
             $sumAmountTaxI = '0.00';                                                                                         // FIELD 22
             $amountOther   = '0.00';                                                                                         // FIELD 23  
             $baseCalICMS   = $item[0]["amount"] + $voidDiscount;                                                             // FIELD 24
             $amountIcms    = number_format(($baseCalICMS*$item[0]["tax_percent"]/100),2,'.','');                       // FIELD 32  
             $amountIcmsST  = '0.00';                                                                                         // FIELD 33
             $baseCalIcmsST = '0.00';                                                                                         // FIELD 39
             $cfop          = (substr($item[0]["pos_id"],0,1) == 'S' ? 5933 : 5102 );                                         // FIELD 44  
             $temp          = explode('.',$item[0]["tax_percent"]);                                                     
             $seqTrib       = (substr($item[0]["pos_id"],0,1) == 'S' ? 06 : $temp[0] );                                       // FIELD 71
             // PESQUISANDO SEQALIQUOTAECF                              
             $searchTrib = str_pad($temp[0], 2, '0',0).substr($temp[1],0,2);                                                             
             $seqTribECF = (substr($item[0]["pos_id"],0,1) == 'S' ? 'S'.$searchTrib : $searchTrib );
             // SQL PESQUISA
             $sql  = "SELECT SEQALIQUOTAECF ";
             $sql .= " FROM CONSINCO.RF_CUPMAQSITTRIB WHERE 1=1 "; 
             $sql .= " AND NROEMPRESA = ".$sale["store_key"]; 
             $sql .= " AND NROMAQUINA = ".$sale["pos_number"]; 
             $sql .= " AND SITTRIBUTARIA = '$seqTribECF'"; 
             $result = SelectOracle($link, $sql, 2);
             $seqTrib = $result["SEQALIQUOTAECF"];
             LogMsg("PESQUISANDO TABELA RF_CUPMAQSITTRIB ...: $sql ");
             LogMsg("DADOS TABELA RF_CUPMAQSITTRIB ...: ALIQUOTA ECF ::: ".$item[0]["pos_id"]."  SEQALIQUOTAECF - CONSINCO ::: ".$result["SEQALIQUOTAECF"]);
             break;
    }
    // QUANTIDADE EMBALAGEM
    $qtdeEmb = ltrim(substr($item[0]["plu_id"],strlen($item[0]["plu_id"]) - 4,4),"0");
    if($qtdeEmb > 1){
      $qtdeItem = ($qtdeEmb * $item[0]["quantity"]);
    }else{
      $qtdeItem = $item[0]["quantity"];
    }
    // ROTINA LOTE DO PRODUTO
    if($seqLot != ''){
      LogMsg("CUPOM COM LOTE ...: $seqLot ");
      $seqLotTemp = explode("|", $seqLot);
      LogMsg("ANALISANDO LOTE DO PRODUTO ...: ".$seqLotTemp[0]);
      if($seqLotTemp[0] == $item[0]["plu_id"]){
        $sql  = "SELECT EST.SEQLOTEESTOQUE, ";
        $sql .= "EST.NROLOTEESTOQUE, ";
        $sql .= "EST.NROEMPRESA, ";
        $sql .= "EST.SEQPRODUTO, ";
        $sql .= "EST.SEQLOCAL, "; 
        $sql .= "EST.ESTQLOTE ";
        $sql .= "FROM CONSINCO.MRL_LOTEESTOQUE EST ";
        $sql .= "INNER JOIN CONSINCO.MRL_LOCAL LOC ON EST.NROEMPRESA = LOC.NROEMPRESA AND EST.SEQLOCAL = LOC.SEQLOCAL "; 
        $sql .= "WHERE EST.NROEMPRESA = ".$sale["store_key"]." "; 
        $sql .= "AND EST.SEQPRODUTO = ".substr($item[0]["plu_id"],0,strlen($item[0]["plu_id"]) - 4)." ";
        $sql .= "AND EST.NROLOTEESTOQUE = '".$seqLotTemp[2]."' ";
        $sql .= "AND LOC.TIPLOCAL = 'L' ";
        $resultLot = SelectOracle($link, $sql, 2);
        $seqLot = $resultLot["SEQLOTEESTOQUE"];
        LogMsg("PESQUISANDO TABELA MRL_LOTEESTOQUE ...: $sql ");
        LogMsg("DADOS TABELA MRL_LOTEESTOQUE ...: NRO LOTE ::: ".$seqLotTemp[2]."  PRODUTO ::: ".$item[0]["plu_id"]."  SEQLOTEESTOQUE :::  ".$seqLot);
      }else{
        $seqLot = '';
      }
    }
    $seqLot = '';    //apresentando erro - verificar 30/11/15
    
    $line90  = "90|";                                                                                   // 1.FIXO 90 - CUPOM FISCAL
    $line90 .= $sale["store_key"]."|";                                                                  // 2.NUMERO DA LOJA 
    $line90 .= $numCupom."|";                                                                           // 3.NUMERO DO CUPOM 
    $line90 .= $serialEcf["ecf_serial"]."|";                                                            // 4.NUMERO DE SERIE ECF 
    $line90 .= "1|";                                                                                    // 5.CRO  
    $line90 .= $sale["pos_number"]."|";                                                                 // 6.NRO. ECF	
    $line90 .= $sale["pos_number"]."|";                                                                 // 7.NRO. CHECKOUT	
    $line90 .= $numCupom."|";                                                                           // 8.COO ATUAL	
    $line90 .= "CF|";                                                                                   // 9.SERIE - CF -> CUPOM FISCAL, NF -> NOTA FISCAL 	
    $line90 .= $statusCupom."|";                                                                        // 10.STATUS CUPOM - C -> CANCELADO, V -> VALIDO  	
    $line90 .= $cliente."|";                                                                            // 11.CODIGO DO CLIENTE
    $line90 .= FormatDate($fiscal_day,7)."|";                                                           // 12.DATA MOVIMENTO DDMMYY
    $line90 .= ($item[0]["sequence"]+1)."|";                                                            // 13.SEQUENCIA DO ITEM
    $line90 .= $statusItem."|";                                                                         // 14.STATUS ITEM - C -> CANCELADO, V -> VALIDO  	
    $line90 .= $item[0]["sku_id"]."|";                                                                  // 15.EAN DO PRODUTO
    $line90 .= $qtdeItem."|";                                                                           // 16.QUANTIDADE
    $line90 .= $qtdeEmb.".000|";                                                                        // 17.QUANTIDADE EMBALAGEM
    $line90 .= substr($item[0]["plu_id"],0,strlen($item[0]["plu_id"]) - 4)."|";                         // 18.CODIGO DO PRODUTO
    $line90 .= number_format(($item[0]["amount"] + $item[0]["discount"]),3,'.','')."|";                 // 19.VALOR DO ITEM
    $line90 .= number_format($item[0]["increase"],3,'.','')."|";                                        // 20.ACRESCIMO DO ITEM
    $line90 .= number_format($itemDiscount,3,'.','')."|";                                               // 21.VALOR DESCONTO
    $line90 .= number_format($sumAmountTaxI,3,'.','')."|";                                              // 22.VALOR TOTAL ISENTO
    $line90 .= number_format($amountOther,3,'.','')."|";                                                // 23.VALOR TOTAL OUTROS IMPOSTOS
    $line90 .= number_format($baseCalICMS,3,'.','')."|";                                                // 24.BASE CALCULO ICMS
    $line90 .= $item[0]["tax_percent"]."|";                                                             // 25.PERCENTUAL ALIQUOTA ICMS
    $line90 .= FormatDate($date,8)."|";                                                                 // 26.DATA-HORA DE EMISSAO DDMMYYHHMMSS
    $line90 .= $seqOperCon5."|";                                                                        // 27.CODIGO DO OPERADOR
    $line90 .= $seqOperCon5."|";                                                                        // 28.CODIGO DO SUPERVISOR
    $line90 .= "N|";                                                                                    // 29.TIPO DE VENDA
    $line90 .= $vendedor."|";                                                                           // 30.CODIGO DO VENDEDOR
    $line90 .= "|";                                                                                     // 31.NRO DA TRIBUTACAO
    $line90 .= number_format($amountIcms,3,'.','')."|";                                                 // 32.VALOR ICMS
    $line90 .= $amountIcmsST."|";                                                                       // 33.VALOR ICMS ST
    $line90 .= "|";                                                                                     // 34.NRO NOTA FISCAL
    $line90 .= "|";                                                                                     // 35.SERIE NOTA FISCAL
    $line90 .= "0.000|";                                                                                // 36.PERCENTUAL ALIQUOTA IPI
    $line90 .= "0.000|";                                                                                // 37.VALOR IPI
    $line90 .= $cgo."|";                                                                                // 38.CGO
    $line90 .= $baseCalIcmsST."|";                                                                      // 39.BASE CALCULO ICMS ST
    $line90 .= "|";                                                                                     // 40.BASE CALCULO FECOP
    $line90 .= "|";                                                                                     // 41.PERCENTUAL ALIQUOTA FECOP
    $line90 .= "|";                                                                                     // 42.VALOR FECOP
    $line90 .= $itemDiscount."|";                                                                       // 43.VALOR DESCONTO
    $line90 .= $cfop."|";                                                                               // 44.CFOP
    $line90 .= "|";                                                                                     // 45.SITUACAO NF
    $line90 .= "N|";                                                                                    // 46.INDICADOR EMISSAO NOTA FISCAL
    $line90 .= "0.00|";                                                                                 // 47.PERCENTUAL ALIQUOTA ICMS ST
    $line90 .= "|";                                                                                     // 48.VALOR TOTAL ISENTO IPI
    $line90 .= "|";                                                                                     // 49.VALOR TOTAL OUTRO IPI
    $line90 .= "08.01.26a|";                                                                            // 50.VERSAO APLICACAO - SOFTWARE
    $line90 .= "|";                                                                                     // 51.NRO DO PEDIDO
    $line90 .= "|";                                                                                     // 52.NRO DA COTACAO
    $line90 .= "|";                                                                                     // 53.QTDE PONTOS
    $line90 .= $history."|";                                                                            // 54.OBSERVACAO
    $line90 .= "|";                                                                                     // 55.OBSERVACAO LIVRO FISCAL
    $line90 .= "|";                                                                                     // 56.SEQUENCIA PROMOCAO PDV
    $line90 .= "|";                                                                                     // 57.NUMERO NSU NOTA FISCAL
    $line90 .= "|";                                                                                     // 58.CPF/CNPJ CLIENTE
    $line90 .= "|";                                                                                     // 59.NOME DO CLIENTE
    $line90 .= "|";                                                                                     // 60.SEQUENCIA REGRA DE INCENTIVO
    $line90 .= "|";                                                                                     // 61.VLR ITEM NORMAL
    $line90 .= "|";                                                                                     // 62.VLR DESCONTO ECF
    $line90 .= "0|";                                                                                    // 63.NRO. BICO
    $line90 .= "|";                                                                                     // 64.CPF/CNPJ CLIENTE
    $line90 .= "0|";                                                                                    // 65.NRO. CARTAO PRESENTE
    $line90 .= "|";                                                                                     // 66.DOTZ
    $line90 .= "|";                                                                                     // 67.DOTZ PONTOS
    $line90 .= $seqLot."|";                                                                             // 68.SEQ LOTE
    $line90 .= "|";                                                                                     // 69.INDICADOR DE NF BASE CP
    $line90 .= "|";                                                                                     // 70.SEQUENCIA ECF
    $line90 .= $seqTrib."|";                                                                            // 71.SEQALIQOTAECF
    
    return $line90;

  }else{
    return -1;
  }
}

function Con5PagamentoEventos($sale, $media, $ticket, $date, $fiscal_day, $typeTicket = 'CF', $typeEvent = 'P', $typeTransaction = 0, $mainLink = -1, $typeName = 'Cupom Fiscal'){

  global $AGREEMENT_MEDIA, $PBM_MEDIA, $TYPE_CARTDEB_RC, $TYPE_CARTCRED_RC, $START_NOTIFICATION, $START_SEND_EMAIL;
  
  // STATUS DO PAGAMENTO - C->CANCELADO, F->FINALIZADO 
  if($sale["voided"] == 1 or $sale["post_sale_void"] == 1){
    $statusPag = 'C'; 
  }else{  
    $statusPag = 'F';
  }
  // ULTIMO CUPOM CANCELADO
  if($sale["post_sale_void"] == 1){ 
    $numCupom = $sale["void_ticket_number"];
  }else{ 
    $numCupom = $sale["ticket_number"];
  }
  // INFORMACOES SOBRE O PAGAMENTO   
  if($typeEvent == 'P'){ 
    $bank       = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 2,  $media[0]["sequence"]);   // NRO. BANCO
    $agency     = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 7,  $media[0]["sequence"]);   // NRO. AGENCIA
    $account    = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 8,  $media[0]["sequence"]);   // NRO. CONTA
    $check      = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 9,  $media[0]["sequence"]);   // NRO. CHEQUE
    $checkDate  = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 10, $media[0]["sequence"]);   // DATA CHEQUE    
    $tefBin     = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 17, $media[0]["sequence"]);   // NRO. BIN
    $tefRed     = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 18, $media[0]["sequence"]);   // NRO. REDE
    $tefParc    = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 53, $media[0]["sequence"]);   // NRO. PARCELAS
    $tefBand    = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 96, $media[0]["sequence"]);   // NRO. BANDEIRA
    $tefNsu     = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 204,$media[0]["sequence"]);   // NRO. NSU
    $tefNsuCorb = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 221,$media[0]["sequence"]);   // NRO. NSU CORRBAN
    $tefBandName= SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 240,$media[0]["sequence"]);   // NOME INSTITUICAO  
    $tefReturn  = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 245,$media[0]["sequence"]);   // RETORNO CONSULTA CHEQUE
    $cmc7       = SelectSaleMediaData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 247,$media[0]["sequence"]);   // NRO. CMC7
  }else{
    $bank       = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 2);                              // NRO. BANCO
    $agency     = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 7);                              // NRO. AGENCIA
    $account    = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 8);                              // NRO. CONTA
    $check      = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 9);                              // NRO. CHEQUE
    $checkDate  = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 10);                             // DATA CHEQUE    
    $tefBin     = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 17);                             // NRO. BIN
    $tefRed     = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 18);                             // NRO. REDE
    $tefParc    = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 53);                             // NRO. PARCELAS
    $tefBand    = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 96);                             // NRO. BANDEIRA
    $tefNsu     = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 203);                            // NRO. NSU
    $tefNsuCorb = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 221);                            // NRO. NSU CORRBAN
    $tefBandName= SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 240);                            // NOME INSTITUICAO  
    $tefReturn  = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 245);                            // RETORNO CONSULTA CHEQUE
    $cmc7       = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 247);                            // NRO. CMC7
  }
  // DADOS GERAIS 
  $codCliente = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 1);                                // CLIENTE 
  $cpfCnpj    = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 3);                                // CPF/CNPJ
  $pbm        = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 306);                              // TIPO PBM
  $agreement  = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 357);                              // NRO. CONVENIO  
  $agreeDate  = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 359);                              // VENCIMENTO CONVENIO  
  $serialEcf  = SerialNumberECF ($ticket["store_key"],$ticket["ecf_number"]);	                                                                       // NRO.SERIE ECF
  $requestJoin = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 6,FALSE, FALSE , " GROUP BY data_value  ORDER BY item_sequence ", 1);  // NRO. PEDIDO 
  // UNIFICACAO DOS PEDIDOS DELIMITADOR -
  $request = ''; 
  for($r = 0; $r < count($requestJoin); $r++){
    $request .= $requestJoin[$r]["data_value"].'-'; 
  }
  $request = substr($request, 0, strlen($request) - 1);
 
  // CONDICOES  
  $cliente    = ($codCliente == '' ? $cpfCnpj : $codCliente);
  $tefNsu     = ($tefNsu == '' ? $tefNsuCorb : $tefNsu);
  $tefStatus  = ($tefNsu == '' ? '' : 'V');  
  $cmc7       = ($cmc7 == '' ? '' : substr($cmc7,2));
  $checkDate  = ($checkDate == '' ? '' : substr($checkDate,0,4).substr($checkDate,6,2));
  $agreeDate  = ($agreeDate == '' ? '' : substr($agreeDate,0,2).substr($agreeDate,3,2).substr($agreeDate,8,2));  

  $history   = "$typeName: [$numCupom]   ORCAMENTO: [$request]   PDV: [".$sale["pos_number"]."]   SOFT:[EMPORIUM]";                 

  // CARTOES SEM BANDEIRA
  if(!$tefBand){
    switch($tefBandName){
      case 'VISA':
        $tefBand = 1;
        break;  
      case 'MASTERCARD':
        $tefBand = 2;
        break;
      case 'REDE BIGCARD':
        $tefBand = 103;
        break;
      case 'SOFTWAY':
        $tefBand = 105;
        break;
      case 'MAESTRO':
        $tefBand = 20001;
        break;
      case 'ELECTRON':
        $tefBand = 20002;
        break;
      case 'CABAL':
        $tefBand = 20003;
        break;
      case 'ELO DEBITO':
        $tefBand = 20032;
        break;
      default:
        $tefBand = 0;
        break;
    }
  }else{
    $tefBand = intval($tefBand);
  }
  // CARTOES SEM REDE
  if(!$tefRed){
    $tefRed = 125;
  }else{
    $tefRed = intval($tefRed);
  }
  // LINK ORACLE
  if($mainLink == -1){
    if(($link = ConnectingInOracle()) == -1){
      LogMsg("FINALIZANDO PROCESSO ::: ERRO CONEXAO ORACLE ");
      return -1;
    }
      LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
  }else{
    LogMsg("USANDO LINK PRINCIPAL DO ORACLE ");
    $link = $mainLink; 
  }
  
  $oper = SelectMysqlOper($sale["cashier_key"]);
  $sql  = "SELECT codopefrentepdv, seqoperador ";
  $sql .= "FROM CONSINCO.mfl_operador ";
  $sql .= "WHERE 1=1 ";
  $sql .= "AND nroempresa = ".$sale["store_key"]." ";
  $sql .= "AND codopefrentepdv = ".$oper["alternate_id"]." ";
  $sql .= "AND statusoperador = 'A' ";
  $result = SelectOracle($link, $sql, 2);
  $seqOperCon5 = $result["SEQOPERADOR"];
  LogMsg("PESQUISANDO BASE CONSINCO...: ".$sql);
  LogMsg("DADOS TABELA MFL_OPERADOR...: OPERADOR EMPORIUM :: ".$result["CODOPEFRENTEPDV"]." CONSINCO :: ".$seqOperCon5);
  if($seqOperCon5 == ''){
    $seqOperCon5 = 92;
    $msgError  = "LOJA : ".$sale["store_key"]."  PDV : ".$sale["pos_number"]."  COO : ".$sale["ticket_number"]." :: ";
    $msgError .= "OPERADOR ".$oper["alternate_id"]." :: USANDO DADOS DEFAULT :: ".$seqOperCon5." MOTIVO :: MOTIVO :: PROBLEMA NO CADASTRO OPERADOR/LOJA NA CON5.";
    LogMsg($msgError);
    // TRATAMENTO DE ALERTAS
    if($START_NOTIFICATION === TRUE){
      $result = InsertNotification($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], 15, 102, $msgError);
    }      
    //if($START_SEND_EMAIL === TRUE){
    //  $send = SendEmail("jarison@remaqbh.com.br", "emporium@drogariaminasbrasil.com.br", "Alertas Integracao MBR", $msg.$msgError, "kerley@remaqbh.com.br");
    //}
  }
  $sessionPos = Con5Session($sale["store_key"], $sale["pos_number"], $sale["fiscal_date"], $sale["ticket_number"], $oper["alternate_id"], 4);  //Field 43    
    
  switch ($media[0]["media_id"]) {
    
    case $AGREEMENT_MEDIA:
          LogMsg("FINALIZADORA CONVENIO");
          // DEFAULT
          $defaultCodMov    = '';
          $defaultFormPagto = $media[0]["media_id"];
          $defaultTefBand   = '';
          $tefRed           = '';
          if($CUSTOMER_CARD === TRUE){
            $sql  = "SELECT PAG.NROFORMAPAGTO, ";
            $sql .= "PAG.FORMAPAGTO, ";
            $sql .= "PAG.SEQPESSOACONVENIO, ";
            $sql .= "PAG.NROFORMAPAGTOECF, ";
            $sql .= "CLI.STATUSCARTAO ";
            $sql .= "FROM CONSINCO.MRL_CLIENTECARTAO CLI, ";
            $sql .= "CONSINCO.MRL_FORMAPAGTO PAG ";
            $sql .= "WHERE CLI.NROCARTAO = $cliente ";
            $sql .= "AND PAG.NROFORMAPAGTOECF = ".$media[0]["media_id"]." ";
            $sql .= "AND CLI.NROFORMAPAGTO = PAG.NROFORMAPAGTO ";
            $sql .= "AND CLI.STATUSCARTAO = 'A' ";
            $resultF = SelectOracle($link, $sql, 2);
            LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
            $typeTransaction = $defaultCodMov;
            $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
            $tefBand         = $defaultTefBand;
            LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }else{
            $sql  = "SELECT CODMOVIMENTO ";
            $sql .= "FROM CONSINCO.FI_TSCODMOVIMENTO ";
            $sql .= "WHERE TIPO = 'CON' ";
            $sql .= "AND NROFORMAPAGTO = ".$agreement;
            $resultF = SelectOracle($link, $sql, 2);
            LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
            if($resultF["CODMOVIMENTO"]  == ''){
              $typeTransaction = $defaultCodMov;
              $nroFormaPagto   = $agreement;
              $tefBand         = $defaultTefBand;
              LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }else{
              $typeTransaction = $resultF["CODMOVIMENTO"];
              $nroFormaPagto   = $agreement;
              LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }
            LogMsg("COD. CONVENIO...: ".$agreement." COD. CONVENIADO...: ".$cliente." COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }
          break;

    case $PBM_MEDIA:
          LogMsg("FINALIZADORA PBM - CONVENIO");
          // DEFAULT
          $defaultCodMov    = '';
          $defaultFormPagto = $media[0]["media_id"];
          $defaultTefBand   = '';
          $tefRed           = '';
          $nroPbmCustomer   = SelectComparison($pbm, 1);     // CONVENIADO
          $nroPbmAgreement  = SelectComparison($pbm, 2);     // CONVENIO
          // SQL
          $sql  = "SELECT CODMOVIMENTO ";
          $sql .= "FROM CONSINCO.FI_TSCODMOVIMENTO ";
          $sql .= "WHERE TIPO = 'CON' ";
          $sql .= "AND NROFORMAPAGTO = ".$nroPbmAgreement["to_data"];
          $resultF = SelectOracle($link, $sql, 2);
          LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
          if($resultF["CODMOVIMENTO"]  == ''){
            $typeTransaction = $defaultCodMov;
            $nroFormaPagto   = $nroPbmAgreement["to_data"];
            $tefBand         = $defaultTefBand;
            LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }else{
            $typeTransaction = $resultF["CODMOVIMENTO"];
            $nroFormaPagto   = $nroPbmAgreement["to_data"];
            $cliente         = $nroPbmCustomer["to_data"];
            LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }
          LogMsg("COD. CONVENIO...: ".$pbm."->".$nroPbm["to_data"]." COD. CONVENIADO...: ".$cliente." COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          break;
          
    case 3:
          LogMsg("FINALIZADORA CREDITO ROTATIVO");
          // DEFAULT
          $defaultCodMov    = 512;
          $defaultFormPagto = 3;
          $defaultTefBand   = 1;
          $tefParc          = 1;
          // SQL
          $sql  = "SELECT PAGECF.NROFORMAPAGTO, ";
          $sql .= "PAGECF.NROFORMAPAGTOECF, ";
          $sql .= "CODMOV.DESCRICAO, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.CODBANDEIRATEF, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.PARCVLRINI, ";
          $sql .= "CODMOV.PARCVLRFIM,  ";
          $sql .= "CODMOV.CODMOVIMENTO ";
          $sql .= "FROM CONSINCO.MRL_FORMAPAGTOECF PAGECF  ";
          $sql .= "LEFT JOIN CONSINCO.FI_TSCODMOVIMENTO CODMOV ON PAGECF.NROFORMAPAGTO = CODMOV.NROFORMAPAGTO ";
          $sql .= "WHERE PAGECF.NROFORMAPAGTOECF = ".$media[0]["media_id"]." ";
          $sql .= "AND PAGECF.NROEMPRESA = ".$media[0]["store_key"]." ";
          $sql .= "AND NVL(CODMOV.CODREDETEF, 0) = '$tefRed' ";
          $sql .= "AND NVL(CODMOV.CODBANDEIRATEF, 0) = '$tefBand' ";
          $sql .= "AND CODMOV.STATUS = 'S' ";
          $resultF = SelectOracle($link, $sql, 2);
          LogMsg("PESQUISANDO BASE CONSINCO...: $sql");

          if($typeTransaction != $TYPE_CARTCRED_RC){
            if($resultF["CODMOVIMENTO"]  == ''){
              $typeTransaction = $defaultCodMov;
              $nroFormaPagto   = $defaultFormPagto;
              $tefBand         = $defaultTefBand;
              LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }else{
              $typeTransaction = $resultF["CODMOVIMENTO"];
              $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
              LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }
          }else{
            if($resultF["CODMOVIMENTO"]  == ''){
              $nroFormaPagto   = $defaultFormPagto;
              $tefBand         = $defaultTefBand;
              LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }else{
              $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
              LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }
          }
          break;

    case 5:
          LogMsg("FINALIZADORA DEBITO");
          // DEFAULT
          $defaultCodMov    = 511;
          $defaultFormPagto = 5;
          $defaultTefBand   = 20002;
          //SQL
          $sql  = "SELECT PAGECF.NROFORMAPAGTO, ";
          $sql .= "PAGECF.NROFORMAPAGTOECF, ";
          $sql .= "CODMOV.DESCRICAO, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.CODBANDEIRATEF, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.PARCVLRINI, ";
          $sql .= "CODMOV.PARCVLRFIM,  ";
          $sql .= "CODMOV.CODMOVIMENTO ";
          $sql .= "FROM CONSINCO.MRL_FORMAPAGTOECF PAGECF  ";
          $sql .= "LEFT JOIN CONSINCO.FI_TSCODMOVIMENTO CODMOV ON PAGECF.NROFORMAPAGTO = CODMOV.NROFORMAPAGTO ";
          $sql .= "WHERE PAGECF.NROFORMAPAGTOECF = ".$media[0]["media_id"]." ";
          $sql .= "AND PAGECF.NROEMPRESA = ".$media[0]["store_key"]." ";
          $sql .= "AND NVL(CODMOV.CODREDETEF, 0) = '$tefRed' ";
          $sql .= "AND NVL(CODMOV.CODBANDEIRATEF, 0) = '$tefBand' ";
          $resultF = SelectOracle($link, $sql, 2);
          LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
          
          if($typeTransaction != $TYPE_CARTDEB_RC){
            if($resultF["CODMOVIMENTO"]  == ''){
              $typeTransaction = $defaultCodMov;
              $nroFormaPagto   = $defaultFormPagto;
              $tefBand         = $defaultTefBand;
              LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }else{
              $typeTransaction = $resultF["CODMOVIMENTO"];
              $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
              LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }
          }else{
            if($resultF["CODMOVIMENTO"]  == ''){
              $nroFormaPagto   = $defaultFormPagto;
              $tefBand         = $defaultTefBand;
              LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }else{
              $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
              LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }
          }
          
          break;

    case 11:
          LogMsg("FINALIZADORA POS CREDITO ROTATIVO");
          // DEFAULT
          $defaultCodMov    = 545;
          $defaultFormPagto = 21;
          $defaultTefBand   = 1;
          // SQL
          $sql  = "SELECT PAGECF.NROFORMAPAGTO, ";
          $sql .= "PAGECF.NROFORMAPAGTOECF, ";
          $sql .= "CODMOV.DESCRICAO, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.CODBANDEIRATEF, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.PARCVLRINI, ";
          $sql .= "CODMOV.PARCVLRFIM,  ";
          $sql .= "CODMOV.CODMOVIMENTO ";
          $sql .= "FROM CONSINCO.MRL_FORMAPAGTOECF PAGECF  ";
          $sql .= "LEFT JOIN CONSINCO.FI_TSCODMOVIMENTO CODMOV ON PAGECF.NROFORMAPAGTO = CODMOV.NROFORMAPAGTO ";
          $sql .= "WHERE PAGECF.NROFORMAPAGTOECF = ".$media[0]["media_id"]." ";
          $sql .= "AND PAGECF.NROEMPRESA = ".$media[0]["store_key"]." ";
          $sql .= "AND NVL(CODMOV.CODREDETEF, 0) = '$tefRed' ";
          $sql .= "AND NVL(CODMOV.CODBANDEIRATEF, 0) = '$tefBand' ";
          $resultF = SelectOracle($link, $sql, 2);
          LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
          if($resultF["CODMOVIMENTO"]  == ''){
            $typeTransaction = $defaultCodMov;
            $nroFormaPagto   = $defaultFormPagto;
            $tefBand         = $defaultTefBand;
            LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }else{
            $typeTransaction = $resultF["CODMOVIMENTO"];
            $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
            LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }
          break;
 
    case 12:
          LogMsg("FINALIZADORA POS DEBITO");
          // DEFAULT
          $defaultCodMov    = 544;
          $defaultFormPagto = 23;
          $defaultTefBand   = 20002;
          // SQL
          $sql  = "SELECT PAGECF.NROFORMAPAGTO, ";
          $sql .= "PAGECF.NROFORMAPAGTOECF, ";
          $sql .= "CODMOV.DESCRICAO, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.CODBANDEIRATEF, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.PARCVLRINI, ";
          $sql .= "CODMOV.PARCVLRFIM,  ";
          $sql .= "CODMOV.CODMOVIMENTO ";
          $sql .= "FROM CONSINCO.MRL_FORMAPAGTOECF PAGECF  ";
          $sql .= "LEFT JOIN CONSINCO.FI_TSCODMOVIMENTO CODMOV ON PAGECF.NROFORMAPAGTO = CODMOV.NROFORMAPAGTO ";
          $sql .= "WHERE PAGECF.NROFORMAPAGTOECF = ".$media[0]["media_id"]." ";
          $sql .= "AND PAGECF.NROEMPRESA = ".$media[0]["store_key"]." ";
          $sql .= "AND NVL(CODMOV.CODREDETEF, 0) = '$tefRed' ";
          $sql .= "AND NVL(CODMOV.CODBANDEIRATEF, 0) = '$tefBand' ";
          $resultF = SelectOracle($link, $sql, 2);
          LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
          if($resultF["CODMOVIMENTO"]  == ''){
            $typeTransaction = $defaultCodMov;
            $nroFormaPagto   = $defaultFormPagto;
            $tefBand         = $defaultTefBand;
            LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }else{
            $typeTransaction = $resultF["CODMOVIMENTO"];
            $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
            LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }
          break;
          
    case 15:
          LogMsg("FINALIZADORA POS CREDITO PARCELADO");
          // DEFAULT
          $defaultCodMov    = 546;
          $defaultFormPagto = 22;
          $defaultTefBand   = 1;
          // SQL
          $sql  = "SELECT PAGECF.NROFORMAPAGTO, ";
          $sql .= "PAGECF.NROFORMAPAGTOECF, ";
          $sql .= "CODMOV.DESCRICAO, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.CODBANDEIRATEF, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.PARCVLRINI, ";
          $sql .= "CODMOV.PARCVLRFIM,  ";
          $sql .= "CODMOV.CODMOVIMENTO ";
          $sql .= "FROM CONSINCO.MRL_FORMAPAGTOECF PAGECF  ";
          $sql .= "LEFT JOIN CONSINCO.FI_TSCODMOVIMENTO CODMOV ON PAGECF.NROFORMAPAGTO = CODMOV.NROFORMAPAGTO ";
          $sql .= "WHERE PAGECF.NROFORMAPAGTOECF = ".$media[0]["media_id"]." ";
          $sql .= "AND PAGECF.NROEMPRESA = ".$media[0]["store_key"]." ";
          $sql .= "AND NVL(CODMOV.CODREDETEF, 0) = '$tefRed' ";
          $sql .= "AND NVL(CODMOV.CODBANDEIRATEF, 0) = '$tefBand' ";
          $sql .= "AND $tefParc ";
          $sql .= "BETWEEN CODMOV.PARCVLRINI and CODMOV.PARCVLRFIM ";
          $resultF = SelectOracle($link, $sql, 2);
          LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
          if($resultF["CODMOVIMENTO"]  == ''){
            $typeTransaction = $defaultCodMov;
            $nroFormaPagto   = $defaultFormPagto;
            $tefBand         = $defaultTefBand;
            LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }else{
            $typeTransaction = $resultF["CODMOVIMENTO"];
            $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
            LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }
          break;

    case 16:
          LogMsg("FINALIZADORA CREDITO PARCELADO");
          // DEFAULT
          $defaultCodMov    = 513;
          $defaultFormPagto = 20;
          $defaultTefBand   = 1;
          // SQL
          $sql  = "SELECT PAGECF.NROFORMAPAGTO, ";
          $sql .= "PAGECF.NROFORMAPAGTOECF, ";
          $sql .= "CODMOV.DESCRICAO, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.CODBANDEIRATEF, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.PARCVLRINI, ";
          $sql .= "CODMOV.PARCVLRFIM,  ";
          $sql .= "CODMOV.CODMOVIMENTO ";
          $sql .= "FROM CONSINCO.MRL_FORMAPAGTOECF PAGECF  ";
          $sql .= "LEFT JOIN CONSINCO.FI_TSCODMOVIMENTO CODMOV ON PAGECF.NROFORMAPAGTO = CODMOV.NROFORMAPAGTO ";
          $sql .= "WHERE PAGECF.NROFORMAPAGTOECF = ".$media[0]["media_id"]." ";
          $sql .= "AND PAGECF.NROEMPRESA = ".$media[0]["store_key"]." ";
          $sql .= "AND NVL(CODMOV.CODREDETEF, 0) = '$tefRed' ";
          $sql .= "AND NVL(CODMOV.CODBANDEIRATEF, 0) = '$tefBand' ";
          $sql .= "AND $tefParc ";
          $sql .= "BETWEEN CODMOV.PARCVLRINI and CODMOV.PARCVLRFIM ";
          $resultF = SelectOracle($link, $sql, 2);
          LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
          if($resultF["CODMOVIMENTO"]  == ''){
            $typeTransaction = $defaultCodMov;
            $nroFormaPagto   = $defaultFormPagto;
            $tefBand         = $defaultTefBand;
            LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }else{
            $typeTransaction = $resultF["CODMOVIMENTO"];
            $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
            LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }
          // Uso temporario  - apresentando problema ao enviar o codigo do movimento correto 27/11/2015
          //$typeTransaction = $defaultCodMov;
          //$nroFormaPagto   = $defaultFormPagto;
          //$tefBand         = $defaultTefBand;
          //LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          // fim do trecho Uso temporario
          
          break;

    default:
          LogMsg("DEFAULT FINALIZADORA");
          // DEFAULT
          $defaultCodMov    = '';
          $defaultFormPagto = $media[0]["media_id"];
          $defaultTefBand   = '';
          $tefRed           = '';
          // SQL
          $sql  = "SELECT PAGECF.NROFORMAPAGTO, ";
          $sql .= "PAGECF.NROFORMAPAGTOECF, ";
          $sql .= "CODMOV.DESCRICAO, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.CODBANDEIRATEF, ";
          $sql .= "CODMOV.CODREDETEF, ";
          $sql .= "CODMOV.PARCVLRINI, ";
          $sql .= "CODMOV.PARCVLRFIM,  ";
          $sql .= "CODMOV.CODMOVIMENTO ";
          $sql .= "FROM CONSINCO.MRL_FORMAPAGTOECF PAGECF  ";
          $sql .= "LEFT JOIN CONSINCO.FI_TSCODMOVIMENTO CODMOV ON PAGECF.NROFORMAPAGTO = CODMOV.NROFORMAPAGTO ";
          $sql .= "WHERE PAGECF.NROFORMAPAGTOECF = ".$media[0]["media_id"]." ";
          $sql .= "AND PAGECF.NROEMPRESA = ".$media[0]["store_key"]." ";
          $resultF = SelectOracle($link, $sql, 2);
          LogMsg("PESQUISANDO BASE CONSINCO...: $sql");
          $nroFormaPagto = $defaultFormPagto;
          if($typeTransaction == 0){
            if($resultF["CODMOVIMENTO"]  == ''){
              $typeTransaction = $defaultCodMov;
              $nroFormaPagto   = $defaultFormPagto;
              $tefBand         = $defaultTefBand;
              LogMsg("DADOS DEFAULT........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }else{
              $typeTransaction = $resultF["CODMOVIMENTO"];
              $nroFormaPagto   = $resultF["NROFORMAPAGTO"];
              LogMsg("DADOS MRL_FORMAPAGTO........: CODIGO EMPORIUM :: ".$media[0]["media_id"]."  CONSINCO :: ".$nroFormaPagto."  COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
            }
          }
          if($typeEvent == 'P'){
            //$typeTransaction = $resultF["CODMOVIMENTO"];    // apresentando problema na consinco quando coloca esse codigo na finalizadora dinheiro ( verificar ???)
            $typeTransaction = '';
            LogMsg("ALTERANDO CODIGO MOVIMENTO PARA TypeEvent == P .: COD. MOVIMENTO TESOURARIA...: ".$typeTransaction);
          }
          break;
  }
  if($mainLink == -1) oci_close($link);           // FECHANDO CONEXAO ORACLE

  $line91  = "91|";                               // 1.FIXO 91 - PAGAMENTO E EVENTOS
  $line91 .= $numCupom."|";                       // 2.COO ATUAL 
  $line91 .= $typeTicket."|";                     // 3.SERIE CF, EV, CB, RC, NF 
  $line91 .= $serialEcf["ecf_serial"]."|";        // 4.NUMERO DE SERIE ECF 
  $line91 .= $media[0]["store_key"]."|";          // 5.NUMERO DA LOJA 
  $line91 .= $media[0]["sequence"]."|";           // 6.SEQUENCIA FINANCEIRO 
  $line91 .= $media[0]["pos_number"]."|";         // 7.NRO CHECKOUT 
  $line91 .= $typeEvent."|";                      // 8.TIPO DO EVENTO 
  $line91 .= $media[0]["amount"]."|";             // 9.VALOR DO LANCAMENTO 
  $line91 .= FormatDate($fiscal_day,7)."|";       // 10.DATA MOVIMENTO DDMMYY
  $line91 .= "0.00|";                             // 11.VALOR DO ACRESCIMO 
  $line91 .= "0.00|";                             // 12.VALOR DO DESCONTO 
  $line91 .= $nroFormaPagto."|";                  // 13.NRO DA FORMA DE PAGAMENTO 
  $line91 .= $statusPag."|";                      // 14.STATUS DO PAGAMENTO 
  $line91 .= $seqOperCon5."|";                    // 15.CODIGO DO OPERADOR 
  $line91 .= $seqOperCon5."|";                    // 16.CODIGO DO SUPERVISOR
  $line91 .= $history."|";                        // 17.HISTORICO DO CHEQUE
  $line91 .= $cliente."|";                        // 18.CPF DO CHEQUE
  $line91 .= $bank."|";                           // 19.NRO DO BANCO
  $line91 .= substr($agency,0,4)."|";             // 20.NRO DA AGENCIA
  $line91 .= $check."|";                          // 21.NRO DO DOCTO - CHEQUE
  $line91 .= $account."|";                        // 22.NRO DA CONTA
  $line91 .= "|";                                 // 23.QTDE DE DOCUMENTO
  $line91 .= "|";                                 // 24.NRO DE VERIFICACAO DO CHEQUE
  $line91 .= $tefNsu."|";                         // 25.NRO NSU TEF
  $line91 .= "N|";                                // 26.TIPO DA COMPRA N -> NORMAL
  $line91 .= $tefStatus."|";                      // 27.SITUACAO TEF V -> VALIDO, C -> CANCELADO 
  $line91 .= "|";                                 // 28.CONFIRMADO S -> SIM, N -> NAO
  $line91 .= "|";                                 // 29.ULTIMOS DIGITOS DO CARTAO
  $line91 .= $tefRed."|";                         // 30.CODIGO DA REDE
  $line91 .= "|";                                 // 31.SEQUENCIAL FINANCEIRO CANCELADO
  $line91 .= FormatDate($date,8)."|";             // 32.DATA E HORA DO LANCAMENTO - DDMMYYHHMMSS
  $line91 .= $agreement."|";                      // 33.SEQUENCIAL DO CONVENIO                                    
  $line91 .= $agreeDate."|";                      // 34.Data do vencimento do pagamento - ddmmyy                            
  $line91 .= $tefParc."|";                        // 35.Nro da parcela
  $line91 .= $agreeDate."|";                      // 36.Data base da cobranca - ddmmyy                                       
  $line91 .= $cmc7."|";                           // 37.CMC7
  $line91 .= "|";                                 // 38.Retorno da consulta de cheque 
  $line91 .= "|";                                 // 39.Supervisor que liberou o cheque
  $line91 .= "|";                                 // 40.Nro nota fiscal
  $line91 .= "|";                                 // 41.Serie nota fiscal
  $line91 .= $cliente."|";                        // 42.Codigo pessoa             
  $line91 .= $sessionPos["session_number"]."|";   // 43.Sequencial do turno do operador
  $line91 .= "|";                                 // 44.Valor gt atual
  $line91 .= "|";                                 // 45.Valor cancelamento atual
  $line91 .= $tefBin."|";                         // 46.BIN do cartao
  $line91 .= $tefBand."|";                        // 47.Codigo da bandeira
  $line91 .= $tefParc."|";                        // 48.Qtde de parcela TEF
  $line91 .= "|";                                 // 49.Valor desconto atual
  $line91 .= "|";                                 // 50.Valor acrescimo atual
  $line91 .= $typeTransaction."|";                // 51.Codigo do movimento
  $line91 .= "|";                                 // 52.Seq. emitente do cheque
  $line91 .= "|";                                 // 53.Valor desconto aniversario
  $line91 .= "|";                                 // 54.Nro do cartao vale presente
  
  return $line91;
}                                                      

function Con5Eventos($store, $pos, $ticket, $date, $cashier, $amountGT = '0.00', $typeTicket = 'EV', $typeEvent = 'F', $amountVoid = 0, $tickets = array()){
  
  global $START_NOTIFICATION, $START_SEND_EMAIL ;
   
  if(($link = ConnectingInOracle()) == -1){
    LogMsg("FINALIZANDO PROCESSO ::: ERRO ORACLE ");
    exit;
  }else{
    LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
    $sql  = "SELECT codopefrentepdv, seqoperador ";
    $sql .= "FROM CONSINCO.mfl_operador ";
    $sql .= "WHERE 1=1 ";
    $sql .= "AND nroempresa = $store ";
    $sql .= "AND codopefrentepdv = $cashier ";
    $sql .= "AND statusoperador = 'A' ";
    $result = SelectOracle($link, $sql, 2);
    $seqOperCon5 = $result["SEQOPERADOR"];
    LogMsg("PESQUISANDO BASE CONSINCO ...: ".$sql);
    LogMsg("Dados da tabela mfl_operador ...: OPERADOR EMPORIUM ::: ".$result["CODOPEFRENTEPDV"]." --- CONSINCO ::: ".$seqOperCon5);
    if($seqOperCon5 == ''){
      $seqOperCon5 = 92;
      $msgError  = "LOJA : ".$store."  PDV : ".$pos."  COO : ".$ticket." :: ";
      $msgError .= "OPERADOR ".$cashier." :: USANDO DADOS DEFAULT :: ".$seqOperCon5." MOTIVO :: PROBLEMA NO CADASTRO OPERADOR/LOJA NA CON5.";
      LogMsg($msgError);
      // TRATAMENTO DE ALERTAS
      if($START_NOTIFICATION === TRUE){
        $result = InsertNotification($store, $pos, $ticket, 15, 102, $msgError);
      }      
      //if($START_SEND_EMAIL === TRUE){
      //  $send = SendEmail("jarison@remaqbh.com.br", "emporium@drogariaminasbrasil.com.br", "Alertas Integracao MBR", $msgError, "kerley@remaqbh.com.br");
      //}
    }
  }
  $serialEcf   = SerialNumberECF ($tickets["store_key"],$tickets["ecf_number"]);                   // Nro.Serie Ecf
  $sessionPos  = Con5Session($store, $pos, $tickets["fiscal_date"], $ticket, $cashier, 4);         //Field 43
  $sumVoid     = $sessionPos["session_cashier_void"];
  $sumDiscount = $sessionPos["session_cashier_discount"];
  if($typeEvent == 'F'){
    $dateMov = str_replace('-', '', $tickets["fiscal_date"]);             // Data YYYMMDD 
  }else{                                                                   
    $dateMov = FormatDate($date,4);                                       // Data YYYMMDD 
  }
  //Inicio linha 91
  $line91  = "91|";                               // 1.Fixo 91 - Pagamento e eventos
  $line91 .= $ticket."|";                         // 2.COO atual 
  $line91 .= $typeTicket."|";                     // 3.Serie CF, EV, CB, RC, NF 
  $line91 .= $serialEcf["ecf_serial"]."|";        // 4.Numero de serie ecf 
  $line91 .= $store."|";                          // 5.Numero da Loja 
  $line91 .= "1|";                                // 6.Sequencia financeiro 
  $line91 .= $pos."|";                            // 7.Nro checkout 
  $line91 .= $typeEvent."|";                      // 8.Tipo do evento 
  $line91 .= "0.00|";                             // 9.Valor do lancamento 
  $line91 .= FormatDate($dateMov,7)."|";          // 10.Data movimento ddmmyy
  $line91 .= "0.00|";                             // 11.Valor do acrescimo 
  $line91 .= "0.00|";                             // 12.Valor do desconto 
  $line91 .= "1|";                                // 13.Nro da forma de pagamento 
  $line91 .= "F|";                                // 14.Status do pagamento 
  $line91 .= $seqOperCon5."|";                    // 15.Codigo do operador 
  $line91 .= $seqOperCon5."|";                    // 16.Codigo do supervisor
  $line91 .= "|";                                 // 17.Historico do cheque
  $line91 .= "|";                                 // 18.CPF do cheque
  $line91 .= "|";                                 // 19.Nro do banco
  $line91 .= "|";                                 // 20.Nro da agencia
  $line91 .= "|";                                 // 21.Nro do docto - cheque
  $line91 .= "|";                                 // 22.Nro da conta
  $line91 .= "1|";                                // 23.Qtde de documento
  $line91 .= "|";                                 // 24.Nro de verificacao do cheque
  $line91 .= "|";                                 // 25.Nro NSU TEF
  $line91 .= "N|";                                // 26.Tipo da compra N -> normal
  $line91 .= "|";                                 // 27.Situacao TEF V -> valido, C -> cancelado 
  $line91 .= "|";                                 // 28.Confirmado S -> sim, N -> nao
  $line91 .= "|";                                 // 29.Ultimos digitos do cartao
  $line91 .= "|";                                 // 30.Codigo da rede
  $line91 .= "|";                                 // 31.Sequencial financeiro cancelado
  $line91 .= FormatDate($date,8)."|";             // 32.Data e hora do lancamento - ddmmyyhhmmss
  $line91 .= "0|";                                // 33.Sequencial do convenio
  $line91 .= "|";                                 // 34.Data do vencimento do pagamento - ddmmyy
  $line91 .= "1|";                                // 35.Nro da parcela
  $line91 .= "|";                                 // 36.CMC7
  $line91 .= "|";                                 // 37.Data base da cobranca - ddmmyy
  $line91 .= "|";                                 // 38.Retorno da consulta de cheque 
  $line91 .= "|";                                 // 39.Supervisor que liberou o cheque
  $line91 .= "|";                                 // 40.Nro nota fiscal
  $line91 .= "|";                                 // 41.Serie nota fiscal
  $line91 .= "|";                                 // 42.Codigo pessoa
  $line91 .= $sessionPos["session_number"]."|";   // 43.Sequencial do turno do operador
  $line91 .= $amountGT."|";                       // 44.Valor gt atual
  $line91 .= $sumVoid."|";                        // 45.Valor cancelamento atual
  $line91 .= "|";                                 // 46.BIN do cartao
  $line91 .= "|";                                 // 47.Codigo da bandeira
  $line91 .= "|";                                 // 48.Qtde de parcela TEF
  $line91 .= $sumDiscount."|";                    // 49.Valor desconto atual
  $line91 .= "|";                                 // 50.Valor acrescimo atual
  $line91 .= "0|";                                // 51.Codigo do movimento
  $line91 .= "|";                                 // 52.Seq. emitente do cheque
  $line91 .= "|";                                 // 53.Valor desconto aniversario
  $line91 .= "|";                                 // 54.Nro do cartao vale presente
  
  return $line91;

}

function Con5ReducaoZ($date, $fiscal_day, $fiscal, $serialEcf, $amountVoidIss=0, $amountDiscIss=0){

  $sumAmountProd    = ($fiscal["final_GT"] - $fiscal["initial_GT"]);                                                              // VALOR BRUTO DOS PRODUTOS       
  $sumAmountProdLiq = ($fiscal["final_GT"] - $fiscal["initial_GT"] - $fiscal["discount"] - $fiscal["void"]);                      // VALOR LIQUIDO DOS PRODUTOS       
  $amountVoidIcms   = $fiscal["void"] - $amountVoidIss;                                                                           // VALOR CANCELAMENTO ICMS
  $amountDiscIcms   = $fiscal["discount"] - $amountDiscIss;                                                                       // VALOR DESCONTO ICMS

  $line92  = "92|";                                                                                                               // 1.FIXO 92 - REDUCAO Z
  $line92 .= $serialEcf["ecf_serial"]."|";                                                                                        // 2.NUMERO DE SERIE ECF 
  $line92 .= FormatDate($fiscal_day,7)."|";                                                                                       // 3.DATA MOVIMENTO DDMMYY
  $line92 .= $fiscal["store_key"]."|";                                                                                            // 4.NUMERO DA LOJA 
  $line92 .= $fiscal["initial_GT"]."|";                                                                                           // 5.GT INICIAL 
  $line92 .= $fiscal["final_GT"]."|";                                                                                             // 6.GT FINAL 
  $line92 .= $fiscal["ticket_number"]."|";                                                                                        // 7.COO ATUAL
  $line92 .= $fiscal["initial_ticket"]."|";                                                                                       // 8.COO INICIAL
  $line92 .= $fiscal["ticket_number"]."|";                                                                                        // 9.COO FINAL
  $line92 .= $fiscal["restart_count"]."|";                                                                                        // 10.CRO 
  $line92 .= $fiscal["Z_number"]."|";                                                                                             // 11.CRZ
  $line92 .= $sumAmountProd."|";                                                                                                  // 12.VALOR BRUTO PRODUTOS
  $line92 .= $amountVoidIcms."|";                                                                                                 // 13.VALOR CANCELAMENTO ICMS
  $line92 .= $fiscal["increase"]."|";                                                                                             // 14.VALOR ACRESCIMO
  $line92 .= $amountDiscIcms."|";                                                                                                 // 15.VALOR DESCONTO ICMS
  $line92 .= $fiscal["pos_number"]."|";                                                                                           // 16.NRO CHECKOUT
  $line92 .= $fiscal["ecf_number"]."|";                                                                                           // 17.NRO ECF
  $line92 .= "|";                                                                                                                 // 18.GT INICIAL CRIPTOGRAFADO
  $line92 .= "|";                                                                                                                 // 19.GT FINAL   CRIPTOGRAFADO
  $line92 .= FormatDate($date,8)."|";                                                                                             // 20.DATA E HORA DE EMISSAO
  
  return $line92;

}

function Con5AliquotasZ($fiscal_day, $fiscalTax, $serialEcf, $mainLink = -1){

  switch ($fiscalTax[0]["pos_id"]) {
    
    case 'F':
            $sitTrib = $fiscalTax[0]["pos_id"];
            $seqTrib = $fiscalTax[0]["pos_id"]."1";
            break;

    case 'I':
            $sitTrib = $fiscalTax[0]["pos_id"];
            $seqTrib = $fiscalTax[0]["pos_id"]."1";
            break;
    
    case 'N':
            $sitTrib = $fiscalTax[0]["pos_id"];
            $seqTrib = $fiscalTax[0]["pos_id"]."1";
            break;

    default:
           // PESQUISANDO SEQALIQUOTAECF                              
           $temp       = explode('.',$fiscalTax[0]["tax_percent"]);                                                     
           $searchTrib = str_pad($temp[0], 2, '0',0).substr($temp[1],0,2);                                                             
           $seqTribECF = (substr($fiscalTax[0]["pos_id"],0,1) == 'S' ? 'S'.$searchTrib : $searchTrib );
           if($mainLink == -1){
             if(($link = ConnectingInOracle()) == -1){
               LogMsg("FINALIZANDO PROCESSO ::: ERRO CONEXAO ORACLE ");
               return -1;
             }
             LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
           }else{
             LogMsg("USANDO LINK PRINCIPAL DO ORACLE ");
             $link = $mainLink; 
           }
           $sql  = "SELECT SEQALIQUOTAECF ";
           $sql .= " FROM CONSINCO.RF_CUPMAQSITTRIB WHERE 1=1 "; 
           $sql .= " AND NROEMPRESA = ".$fiscalTax[0]["store_key"]; 
           $sql .= " AND NROMAQUINA = ".$fiscalTax[0]["pos_number"]; 
           $sql .= " AND SITTRIBUTARIA = '$seqTribECF'"; 
           $result  = SelectOracle($link, $sql, 2);
           if($mainLink == -1) oci_close($link);                                // FECHANDO CONEXAO ORACLE
           $sitTrib = $seqTribECF;
           $seqTrib = $result["SEQALIQUOTAECF"];
           break;
  }
  LogMsg("PESQUISANDO TABELA RF_CUPMAQSITTRIB ...: $sql ");
  LogMsg("DADOS TABELA RF_CUPMAQSITTRIB ...: ALIQUOTA ECF ::: $sitTrib  SEQALIQUOTAECF - CONSINCO ::: $seqTrib ");

  $line93  = "93|";                                                             // 1.FIXO 93 - ALIQUOTAS DA REDUCAO Z
  $line93 .= $serialEcf["ecf_serial"]."|";                                      // 2.NUMERO DE SERIE ECF 
  $line93 .= FormatDate($fiscal_day,7)."|";                                     // 3.DATA MOVIMENTO DDMMYY
  $line93 .= $fiscalTax[0]["store_key"]."|";                                    // 4.NUMERO DA LOJA 
  $line93 .= $sitTrib."|";                                                      // 5.SITUACAO TRIBUTARIA 
  $line93 .= $fiscalTax[0]["amount"]."|";                                       // 6.VALOR TOTAL 
  $line93 .= $fiscalTax[0]["tax_amount"]."|";                                   // 7.VALOR IMPOSTO
  $line93 .= $fiscalTax[0]["tax_percent"]."|";                                  // 8.PERCENTUAL ALIQUOTA
  $line93 .= $seqTrib."|";                                                      // 9.SEQUENCIADA ALIQUOTA
  
  return $line93;   

}

function Mrlx_pdvimportacao($line, $typeFile){
      
  global $store, $ini;

  $fields  = 'nroempresa, ';
  $fields .= 'softpdv, ';
  $fields .= 'dtamovimento, ';
  $fields .= 'dtahorlancamento, ';
  $fields .= 'linha, ';
  $fields .= 'arquivo '; 

  $insert  = 'INSERT INTO CONSINCO.mrlx_pdvimportacao ';
  $insert .= '('.$fields.') ';
  $insert .= 'VALUES ';  
  $insert .= '( ';
  $insert .= $store.', ';
  $insert .= "'".$ini["SOFTPDV"]."', ";
  $insert .= "trunc(sysdate), ";
  $insert .= "sysdate, ";
  $insert .= "'".$line."', ";
  $insert .= "'".$typeFile."'";
  $insert .= ') ';

  return $insert;
} 

function Mrl_logexportacao(){
      
  global $store, $ini;

  $fields  = 'nroempresa, ';
  $fields .= 'softpdv, ';
  $fields .= 'dtahorexportacao, ';
  $fields .= 'usuexportacao, ';
  $fields .= 'dtamovimento, ';
  $fields .= 'tipolog, ';
  $fields .= 'tipoexportacao '; 

  $insert  = 'INSERT INTO CONSINCO.mrl_logexportacao ';
  $insert .= '('.$fields.') ';
  $insert .= 'VALUES ';  
  $insert .= '( ';
  $insert .= $store.', ';
  $insert .= "'".$ini["SOFTPDV"]."', ";
  $insert .= "sysdate, ";
  $insert .= "'CONSINCO', ";
  $insert .= "sysdate, ";
  $insert .= "'I', ";
  $insert .= "'I'";
  $insert .= ') ';

  return $insert;
} 

function SelectIntegrationCon5($store){
  
  $sql  = "SELECT status FROM integration_con5 WHERE 1=1 ";
  $sql .= "AND store_key = $store ";
  $data = ConnectingInMysql($sql, 2);
  return $data;
}

function Catalog ( $store, $pos, $ticket, $datetime, $type = 0, $status = 0, $file_name = ' ', $amountS01 = 0, $amountS02 = 0, $amountS03 = 0 ) {
          
  switch ($type) {
    case 0:
      $sql  = "SELECT * ";
      $sql .= "FROM catalog_con5 ";
      $sql .= "WHERE catalog_store_key = $store ";
      $sql .= "AND catalog_pos_number = $pos ";
      $sql .= "AND catalog_ticket_number = $ticket ";
      $sql .= "AND catalog_start_time = '$datetime' ";
      $sql .= "ORDER BY catalog_start_time DESC ";
      $sql .= "LIMIT 1";
      LogMsg("PESQUISANDO TABELA CATALOG_CON5 :: $sql");
      $data = ConnectingInMysql($sql, 2);
      if(count($data) == 0) $data = ''; 
      break;
    case 1:
      $sql  = "INSERT INTO "; 
      $sql .= " catalog_con5 ";
      $sql .= " ( ";
      $sql .= " catalog_store_key, ";
      $sql .= " catalog_pos_number, ";
      $sql .= " catalog_ticket_number, ";
      $sql .= " catalog_start_time, ";
      $sql .= " catalog_process_time, ";
      $sql .= " catalog_status, ";
      $sql .= " catalog_tax_S01, ";
      $sql .= " catalog_tax_S02, ";
      $sql .= " catalog_tax_S03, ";
      $sql .= " catalog_xml ";
      $sql .= " ) ";
      $sql .= " VALUES ";
      $sql .= " ( ";
      $sql .= $store.", ";
      $sql .= $pos.", ";
      $sql .= $ticket.", ";
      $sql .= "'".$datetime."', ";
      $sql .= "now(), ";
      $sql .= $status.", ";
      $sql .= "0, ";
      $sql .= "0, ";
      $sql .= "0, ";
      $sql .= "'".$file_name."' ";
      $sql .= " ) ";
      LogMsg("XML CATALOGADO :: $sql");
      $data = ConnectingInMysql($sql, 3);
      break;
    case 2:
      $sql  = "UPDATE catalog_con5 ";
      $sql .= " SET "; 
      $sql .= " catalog_tax_S01 = catalog_tax_S01 + $amountS01, "; 
      $sql .= " catalog_tax_S02 = catalog_tax_S02 + $amountS02, ";
      $sql .= " catalog_tax_S03 = catalog_tax_S03 + $amountS03 ";
      $sql .= " WHERE catalog_store_key = ".$store;
      $sql .= " AND catalog_pos_number = ".$pos;
      $sql .= " AND catalog_ticket_number = '".$ticket."'";
      $sql .= " AND catalog_start_time = '".$datetime."'";
      LogMsg("ATUALIZANDO DADOS ISS TABELA CATALOG_CON5 :: $sql");
     $data = ConnectingInMysql($sql, 3);
      break;
    case 3:
      $sql  = "DELETE ";
      $sql .= " FROM catalog_con5 ";
      $sql .= " WHERE catalog_store_key = ".$store;
      $sql .= " AND catalog_pos_number = ".$pos;
      $sql .= " AND catalog_start_time = '".$datetime."'";
      LogMsg("EXCLUINDO XML :: CATALOG_CON5 :: $sql");
      $data = ConnectingInMysql($sql, 2);
      if(count($data) == 0) $data = ''; 
      break;
    default:
      $sql  = "SELECT * ";
      $sql .= "FROM catalog_con5 ";
      $sql .= "WHERE catalog_store_key = $store ";
      $sql .= "AND catalog_pos_number = $pos ";
      $sql .= "AND catalog_ticket_number = $ticket ";
      $sql .= "AND catalog_start_time = '$datetime' ";
      $sql .= "ORDER BY catalog_start_time DESC ";
      $sql .= "LIMIT 1";
      LogMsg("PESQUISANDO TABELA CATALOG_CON5 :: $sql");
      $data = ConnectingInMysql($sql, 2);
      if(count($data) == 0) $data = ''; 
      break;
  } 
  return $data;
}

//*****************************************************************
//***************  END FUNCTION INTEGRATION CON5  ***************
//*****************************************************************


//*****************************************************************
//***************  BEGIN FUNCTION INTEGRATION EMPORIUM ************
//*****************************************************************

function SelectSale($store, $pos, $ticket, $date){
  
  $sql  = "SELECT * FROM sale WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $data = ConnectingInMysql($sql, 2);
  return $data;
}

function SelectSaleItem($store, $pos, $ticket, $date){
  
  $sql  = "SELECT * FROM sale_item WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $sql .= " ORDER BY sequence ";
  $data = ConnectingInMysql($sql, 1);
  return $data;
}

function SelectSaleMedia($store, $pos, $ticket, $date){
  
  $sql  = "SELECT * FROM sale_media WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $sql .= " ORDER BY sequence ";
  $data = ConnectingInMysql($sql, 1);
  return $data;
}

function SelectSaleMediaGroup($store, $pos, $ticket, $date, $group = 'media_id'){
  
  $sql  = "SELECT ";
  $sql .= " store_key, ";
  $sql .= " pos_number, ";
  $sql .= " ticket_number, ";
  $sql .= " start_time, ";
  $sql .= " sequence, ";
  $sql .= " media_id, ";
  $sql .= " sum(amount) AS amount ";
  $sql .= " FROM sale_media WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $sql .= " GROUP BY $group ";
  $sql .= " ORDER BY sequence ";
  $data = ConnectingInMysql($sql, 1);
  return $data;
}

function SelectSaleMediaData($store, $pos, $ticket, $date, $dataId, $sequence, $seqVoided = 9999){
  
  $sql  = "SELECT * FROM sale_media_data WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $sql .= " AND data_id = ".$dataId;
  $sql .= " AND sequence = ".$sequence;
  if($seqVoided != 9999){ 
    $sql .= " AND data_value = ".$sequence;
    LogMsg("ITEM MEDIA_DATA :: $sql");
  }
  $data = ConnectingInMysql($sql, 2);
  if(count($data) == 0) $data = ''; else $data = $data["data_value"];
  return $data;
}

function SelectAnswerData($store, $pos, $ticket, $date, $dataId, $sequence = FALSE, $itemSequence = FALSE, $orderBy = FALSE, $join = FALSE){
  
  $sql  = "SELECT * FROM answer_data WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $sql .= " AND data_id = ".$dataId;
  $sql .= ($sequence !== FALSE ? " AND data_value = ".$sequence : ''); 
  $sql .= ($itemSequence !== FALSE ? " AND item_sequence <= ".$itemSequence : ''); 
  $sql .= ($orderBy != FALSE ? $orderBy : ''); 
  LogMsg("ITEM ANSWER :".$sql);
  if($join != FALSE){
    $data = ConnectingInMysql($sql, 1);
  }else{
    $data = ConnectingInMysql($sql, 2);
    if(count($data) == 0) 
      $data = ''; 
    else 
      $data = $data["data_value"];
  }
  return $data;
}

function SelectTicket($store, $pos, $ticket, $date){
  
  $sql  = "SELECT * FROM ticket WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $data = ConnectingInMysql($sql, 2);
  return $data;
}

function SelectFiscalStatus($store, $pos, $ticket, $date){
  
  $sql  = "SELECT * FROM fiscal_status WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $data = ConnectingInMysql($sql, 2);
  return $data;
}

function SelectFiscalTaxStatus($store, $pos, $ticket, $date){
  
  $sql  = "SELECT * FROM fiscal_tax_status WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND start_time = '".$date."' ";
  $data = ConnectingInMysql($sql, 1);
  return $data;
}

function SelectNotification($store, $pos, $ticket, $transaction=12){

  $sql  = "SELECT * FROM notification WHERE 1=1 ";
  $sql .= " AND store_key = ".$store." ";
  $sql .= " AND pos_number = ".$pos." ";
  $sql .= " AND ticket_number = ".$ticket." ";
  $sql .= " AND transaction_type = ".$transaction;
  $sql .= " ORDER BY notification_time DESC LIMIT 1";
  LogMsg("SQL TABLE NOTIFICATION :: $sql");
  $data = ConnectingInMysql($sql, 2);
  if(count($data) == 0) {
    $data = ''; 
    return $data;
  }else{
    if($transaction == 12 or $transaction == 71){
      $result  = stripos($data["notification_data"], "Entrada");
      if($result !== FALSE){
        $data["status"] = 'Entrada';  
      }
      $result1 = stripos($data["notification_data"], "Saida");
      if($result1 !== FALSE){
        $data["status"] = 'Saida';  
      }
    }
    return $data;
  }
}

function InsertNotification($store, $pos, $ticket, $cashier=15, $transaction=102, $message='Mensagem Padrao'){

  $sql  = "INSERT INTO "; 
  $sql .= " notification ";
  $sql .= " ( ";
  $sql .= " store_key, ";
  $sql .= " pos_number, ";
  $sql .= " notification_time, ";
  $sql .= " notification_type, ";
  $sql .= " transaction_type, ";
  $sql .= " status, ";
  $sql .= " units, ";
  $sql .= " process_id, ";
  $sql .= " ticket_number, ";
  $sql .= " trn_number, ";
  $sql .= " cashier_key, ";
  $sql .= " authorizer_key, ";
  $sql .= " notification_data ";
  $sql .= " ) ";
  $sql .= " VALUES ";
  $sql .= " ( ";
  $sql .= $store.", ";
  $sql .= $pos.", ";
  $sql .= "now(), ";
  $sql .= "0, ";
  $sql .= $transaction.", ";
  $sql .= "NULL, ";
  $sql .= "NULL, ";
  $sql .= "NULL, ";
  $sql .= $ticket.", ";
  $sql .= $ticket.", ";
  $sql .= $cashier.", ";
  $sql .= "15, ";
  $sql .= "'$message' ";
  $sql .= " ) ";
  LogMsg("NOTIFICACAO CATALOGADO :: $sql");
  $data = ConnectingInMysql($sql, 3);

  return $data;
}

function SerialNumberECF($store,$numberEcf){
  
  $sql  = "SELECT ecf_serial FROM ecf ";
  $sql .= "WHERE store_key = ".$store;
  $sql .= " AND ecf_number = ".$numberEcf;
  $data = ConnectingInMysql($sql, 2);
  LogMsg("ECF :: $sql ");
  return $data;
}

function SelectMysqlClerk($codClerk){
 
  $sql  = "SELECT agent_key ";
  $sql .= "FROM user ";
  $sql .= "WHERE alternate_id = ".$codClerk;
  $data = ConnectingInMysql($sql, 2);
  //$data = ($data["agent_key"] != '' ? $data["agent_key"] : '');
  return $data;
}

function SelectMysqlOper($codIntOper){
 
  $sql  = "SELECT alternate_id ";
  $sql .= "FROM user ";
  $sql .= "WHERE agent_key = ".$codIntOper;
  $data = ConnectingInMysql($sql, 2);
  return $data;
}

function SelectMysqlOperName($nameOper){
 
  $sql  = "SELECT agent_key, name ";
  $sql .= "FROM agent ";
  $sql .= "WHERE name LIKE '%".$nameOper."%' ";
  $sql .= "LIMIT 1";
  $data = ConnectingInMysql($sql, 2);   
  return $data;
}

function SelectFiscalAccumItem($store, $pos, $date, $tax=0){
 
  $sql  = "SELECT ";
  $sql .= "SUM(amount) AS amount, ";
  $sql .= "SUM(amount_canc) AS amount_canc, ";
  $sql .= "pos_id, ";
  $sql .= "tax_percent, ";
  $sql .= "store_key, ";
  $sql .= "ecf_number ";
  $sql .= "FROM fiscal_accum_item ";
  $sql .= "WHERE 1=1 ";
  $sql .= "AND store_key = $store ";
  $sql .= "AND ecf_number = $pos ";
  $sql .= "AND fiscal_date = '$date' ";
  $sql .= ($tax == 0 ? '' : "AND pos_id = '$tax' " );   
  $sql .= "GROUP BY ";
  $sql .= "store_key, ";
  $sql .= "ecf_number, ";
  $sql .= "pos_id ";
  $sql .= "ORDER by ";
  $sql .= "store_key, ";
  $sql .= "ecf_number, ";
  $sql .= "pos_id ";
  $data = ConnectingInMysql($sql, 1);   
  return $data;
}

function SelectComparison($search, $type=1){

  $sql  = "SELECT ";
  $sql .= "to_data ";
  $sql .= "FROM comparison ";
  $sql .= "WHERE 1=1 ";
  $sql .= "AND f1_data = '$search' ";
  $sql .= "AND type = $type ";
  $data = ConnectingInMysql($sql, 2);   
  return $data;
}

function SelectPos($store){

  $sql  = "SELECT ";
  $sql .= "pos_number ";
  $sql .= "FROM pos ";
  $sql .= "WHERE 1=1 ";
  $sql .= "AND store_key = $store ";
  $sql .= "AND pos_number < 999 ";
  $data = ConnectingInMysql($sql, 1);   
  return $data;
}


//*****************************************************************
//***************  END FUNCTION INTEGRATION EMPORIUM **************
//*****************************************************************


//*****************************************************************
//***************  BEGIN FUNCTION CONTROL AND SERVICES ************
//*****************************************************************

function ConnectingInMysql($sql, $type = 1) {
   
  // TYPE
  // 1 -> MATRIZ
  // 2 -> ARRAY

  global $ini, $DATABASE;

  $link = mysql_connect($ini["MYSQLHOST"], $ini["MYSQLUSER"], $ini["MYSQLPASS"]); 
  if(!$link) {
    LogMsg ("NAO FOI POSSIVEL CONECTAR NO MYSQL.");
    LogMsg (" [ ".$ini["MYSQLHOST"]." ][ ".$ini["MYSQLUSER"]." ] \n");
    return -1;
  }
  if(!mysql_select_db($DATABASE)) {
    LogMsg ("ERROR ::: ".mysql_error());
    $send = SendEmail("jarison@remaqbh.com.br", "emporium@drogariaminasbrasil.com.br", "Alertas Integracao MBR", "ERROR :: ".mysql_error(), "kerley@remaqbh.com.br");    
    return -1;
  }
  $res = mysql_query($sql, $link);
  if(!$res){ 
    LogMsg ("ERROR SQL ::: $sql ");
    $send = SendEmail("jarison@remaqbh.com.br", "emporium@drogariaminasbrasil.com.br", "Alertas Integracao MBR", "ERROR :: ".$sql, "kerley@remaqbh.com.br");    
    return -1;
  }
  $data = array();
  LogMsg ("INICIALIZANDO ARRAY DE DADOS.   SQL :::  $sql   BANCO :::  $DATABASE ");
  
  switch($type){ 
    case 1:
      $i = 0;
      while($row = mysql_fetch_array($res)){
        $data[$i] = $row;
        $i++;
      }
      break;

    case 2:
      $data = mysql_fetch_array($res);
      break;

    case 3:
      $data = 0;
      break;

  } 
  mysql_free_result($res);    // LIBERANDO MEMORIA
  mysql_close($link);         //FECHANDO CONEXAO MYSQL

  return $data;
}

function ConnectingInOracle() {

  global $ini;

  $link = oci_connect(
	  $ini["ORACLEUSER"], $ini["ORACLEPASS"]," 
	  (DESCRIPTION =
          (ADDRESS = (PROTOCOL = TCP)
          (HOST = ".$ini["ORACLEHOST"].")(PORT =".$ini["ORACLEPORT"]."))
          (CONNECT_DATA =
          (SERVER = ".$ini["ORACLESERVER"].")
          (SERVICE_NAME =".$ini["ORACLESERVICE"].")))");
  if(!$link){
    $msg = oci_error();
    LogMsg("ERRO ORACLE ::: ".$msg['message']."\n");
    $send = SendEmail("jarison@remaqbh.com.br", "emporium@drogariaminasbrasil.com.br", "Alertas Integracao MBR", $msg['message'], "kerley@remaqbh.com.br");    
    return -1;
  }
  $result = InsertOracle($link, 1, "alter session set NLS_NUMERIC_CHARACTERS='. '");
  //COMMIT
  $result = InsertOracle($link, 2);
  return $link;
}

function SelectOracle($link, $sql, $type = 1){
  
  $validation = oci_parse($link, $sql);
  $res = oci_execute ($validation , OCI_DEFAULT );
  if(!$res){
    LogMsg("\n".oci_error($validation)."\n");
    return -1;
  }else{
    switch($type){ 
      case 1:
        $i = 0;
        $data = Array();
        while(($row = oci_fetch_array($validation, OCI_BOTH))) {
          $data[$i] =  $row;
          $i++;
        }
        break;
             
      case 2:
        $data = oci_fetch_array($validation, OCI_BOTH);
        break;
    }       
    //Liberando memoria
    oci_free_statement($validation);
    return($data);
  }
}

function InsertOracle($link, $type = 1, $sql){
  
  switch($type){ 
    case 1:
      $validation = oci_parse($link, $sql);
      $res = oci_execute ($validation , OCI_DEFAULT );
      if(!$res){
        $failure = oci_error($validation);
        LogMsg($failure["message"]."\n  SQL :::::: ".$failure["sqltext"]);
        return -1;
      }else{
        LogMsg("SQL :::::: $sql");
        //Liberando memoria
        oci_free_statement($validation);
        return 1;
      }
      break;
             
    case 2:
      oci_commit($link); 
      LogMsg("COMMIT SQL :::::: ");
      return 1;
      break;
    
    default :
      return -1;
      break;
  }       
}

function LogMsg($msg) {

  global $store;
  
  //$logFile   = $ini['LOG_DIR']."integra_exp_consinco".str_pad($store, 4, "0", "0").".log";
  //$logFile   = "/var/log/integra_con5_exp_".str_pad($store, 4, "0", "0")."_".date('Y-m-d').".log";
  $logFile   = "/var/log/integra_con5_exp_".str_pad($store, 4, "0", "0").".log";
  $dateProc  = date("Y-m-d H:i:s")." -> ";
  $trace = fopen($logFile, "a");
  if(!$trace)
    return -1;
  fputs($trace, $dateProc.$msg."\n");
  fclose($trace);
  return 0;
}

function CheckParameter(){
  global $argv;
  if ( !isset($argv[1]) ){
    LogMsg("PRM LOJA OBRIGATORIO");
    return FALSE;
  }
  if ( $argv[1] == '0' ){
    LogMsg("LOJA COM NUMERO IGUAL A ZERO");
    return FALSE;
  }
  if ( !isset($argv[2])){
    LogMsg("PRM PDV OBRIGATORIO");
    return FALSE;
  }
  if ( $argv[2] == '0' ){
    LogMsg("PDV COM NUMERO IGUAL A ZERO");
    return FALSE;
  }
  if ( !isset($argv[3]) ){
    LogMsg("PRM TICKET OBRIGATORIO");
    return FALSE;
  }
  if ( $argv[3] == '0' ){
    LogMsg("TICKET COM NUMERO IGUAL A ZERO");
    return FALSE;
  }
  if ( !isset($argv[4]) ){
    LogMsg("PRM DATA OBRIGATORIO");
    return FALSE;
  }
  if ( !isset($argv[5]) ){
    LogMsg("PRM ARQUIVO OBRIGATORIO");
    return FALSE;
  }
  return TRUE;
}

function ReadXML ($arr, $tag){
   $qty = count ($arr);
   $il = 0;
   while ($il <= $qty){ // W
     $line = $arr[$il];

     if (substr_count ($line, $tag) > 0){
        $xf = strpos($line, $tag);
        $pf = substr($line, $xf);
        $vf = substr ($pf, strlen($tag), strpos($pf, "/") - 2);
        $content = strip_tags($vf);
        LogMsg ("CONTENT TAG [ $tag ] -> $content");

        return $content;
     }
     $il++;
    } // W -END
    return "";
}

function FormatDate($date, $format=0){

  $year   = substr ($date, 0, 4);    
  $yshort = substr ($date, 2, 2);                 
  $month  = substr ($date, 4, 2);    
  $day    = substr ($date, 6, 2);       
  $hours  = substr ($date, 8, 2);      
  $min    = substr ($date, 10, 2);                 
  $seg    = substr ($date, 12, 2);  
  
  switch ($format) {
    case 0:
      $change = $year."-".$month."-".$day; // FORMATO: YYYY-MM-DD
      break;
    case 1:
      $change = $year."-".$month."-".$day." ".$hours.":".$min.":".$seg; // FORMATO: YYYY-MM-DD HH:II:SS
      break;
    case 2:
      $change = $hours.":".$min.":".$seg; // FORMATO: HH:II:SS
      break;
    case 3:
      $change = $year.$month.$day.$hours.$min.$seg; // FORMATO: YYYYMMDDHHIISS
      break;
    case 4:
      $change = $year.$month.$day; // FORMATO: YYYYMMDD
      break;
    case 5:
      $change = $yshort.$month.$day; // FORMATO: YYMMDD
      break;
    case 6:
      $change = $hours.$min.$seg;  // FORMATO: HHIISS
      break;
    case 7:
      $change = $day.$month.$yshort;  // FORMATO: DDMMYY
      break;
    case 8:
      $change = $day.$month.$yshort.$hours.$min.$seg;  // FORMATO: DDMMYYHHMMSS
      break;
    default:
      $change = $year."-".$month."-".$day; // FORMATO: YYYY-MM-DD
      break;
  }
  return $change;
}

function LoadingConfiguration($fileName){
      	
  if (file_exists($fileName)){
    $ini = parse_ini_file($fileName);
  }else{
    return -1;
  }
  return $ini; 
}

function CreateDirectory($path, $type = 0){

  switch($type){
    case 0:
      if(!is_dir(substr($path, 0, strlen($path) - 9))){
        LogMsg("CRIANDO DIR DATA :::  ".(substr($path, 0, strlen($path) - 9)));
        mkdir ((substr($path, 0, strlen($path) - 9)), 0777);
      }
      if(!is_dir(substr($path, 0, strlen($path) - 4))){
        LogMsg("CRIANDO DIR LOJA :::  ".(substr($path, 0, strlen($path) - 4)));
        mkdir ((substr($path, 0, strlen($path) - 4)), 0777);
      }
      if(!is_dir($path)){
        LogMsg("CRIANDO DIR PDV :::  $path");
        mkdir ($path, 0777);
      }
      break;
    
    case 1:
      if(!is_dir(substr($path, 0, strlen($path) - 16))){
        LogMsg("CRIANDO DIR  :::  ".(substr($path, 0, strlen($path) - 16)));
        mkdir ((substr($path, 0, strlen($path) - 16)), 0777);
      }
      if(!is_dir($path)){
        LogMsg("CRIANDO DIR  :::  $path");
        mkdir ($path, 0777);
      }
      break;
  }
  return 0;
}

function RenameFile($fileName){
  $path = dirname($fileName);
  $name = basename($fileName);

  if(substr($name,0,5) == "WAIT_"){
    $name = substr($name,12);
    //sleep(1);
  }
  $newFileName = $path."/WAIT_".date("his")."_".$name;
  if(copy($fileName, $newFileName)){
    LogMsg("COPIANDO PARA ::: $fileName  PARA  ::: $newFileName");
    return TRUE;
  }else{
    LogMsg("COPIANDO PARA ::: $fileName  PARA  ::: $newFileName");
    return FALSE;
  }
  return TRUE;
}

function CreateXml ( $store, $pos, $fiscal_date) {

  global $ini;
  
  $sql  = "SELECT ";
  $sql .= " store_key, ";
  $sql .= " pos_number, ";
  $sql .= " ticket_number, ";
  $sql .= " date_format(start_time, '%Y%m%d%H%i%s') AS start_time_alt, ";
  $sql .= " start_time, ";  
  $sql .= " ticket_type, ";
  $sql .= " trn_number, ";
  $sql .= " date_format(fiscal_date, '%Y%m%d') AS fiscal_date_alt, ";
  $sql .= " fiscal_date ";    
  $sql .= " FROM ticket ";
  $sql .= " WHERE store_key = ".$store;
  $sql .= ($pos == 999 ? " AND pos_number > 0 " : " AND pos_number = ".$pos ); 
  $sql .= " AND fiscal_date = '".$fiscal_date."'";
  $sql .= " ORDER BY store_key, pos_number, ticket_number";
  $data = ConnectingInMysql($sql, 1);
  
  for($i = 0; $i < count($data); $i++){
    
    $record = 1;
    switch($data[$i]["ticket_type"] ){
    
      case 0:
        LogMsg("CRIANDO XML VENDA");
        
        $dataS= SelectSale($data[$i]["store_key"], $data[$i]["pos_number"], $data[$i]["ticket_number"], $data[$i]["start_time"]); 
        $oper = SelectMysqlOper($dataS["cashier_key"]); 
        
        $xml  = "<?xml version='1.0' encoding='ISO-8859-1' ?-> \n";
        $xml .= "<SALE> \n";
        $xml .= "<STORE>".$data[$i]["store_key"]."</STORE><POS>".$data[$i]["pos_number"]."</POS><TICKET>".$data[$i]["ticket_number"]."</TICKET><TRN>".$data[$i]["trn_number"]."</TRN> \n";
        $xml .= "<FISCAL_STORE>".$data[$i]["store_key"]."</FISCAL_STORE><FISCAL_POS>".$data[$i]["pos_number"]."</FISCAL_POS> \n";
        $xml .= "<CASHIER_ID>".$oper["alternate_id"]."</CASHIER_ID> \n";
        $xml .= "<FISCAL_DAY>".$data[$i]["fiscal_date_alt"]."</FISCAL_DAY><FISCAL_TIME>".$data[$i]["start_time_alt"]."</FISCAL_TIME> \n";
        $xml .= "</SALE>";
        
        break;

      case 1:
        LogMsg("CRIANDO XML LEITURA Z");

        $dataX= SelectFiscalStatus($data[$i]["store_key"], $data[$i]["pos_number"], $data[$i]["ticket_number"], $data[$i]["start_time"]);          
        
        $xml  = "<?xml version='1.0' encoding='ISO-8859-1' ?-> \n";
        $xml .= "<Z_REPORT> \n";
        $xml .= "<STORE>".$data[$i]["store_key"]."</STORE><POS>".$data[$i]["pos_number"]."</POS><TICKET>".$data[$i]["ticket_number"]."</TICKET><TRN>".$data[$i]["trn_number"]."</TRN> \n";
        $xml .= "<FISCAL_STORE>".$data[$i]["store_key"]."</FISCAL_STORE><FISCAL_POS>".$data[$i]["pos_number"]."</FISCAL_POS> \n";
        //$xml .= "<CASHIER_ID>".$oper["alternate_id"]."</CASHIER_ID> \n";
        $xml .= "<CASHIER_ID>92</CASHIER_ID> \n";
        $xml .= "<FISCAL_DAY>".$data[$i]["fiscal_date_alt"]."</FISCAL_DAY><FISCAL_TIME>".$data[$i]["start_time_alt"]."</FISCAL_TIME> \n";
        $xml .= "<INITIAL_GT>".$dataX["initial_GT"]."</INITIAL_GT> \n";
        $xml .= "<FINAL_GT>".$dataX["final_GT"]."</FINAL_GT> \n";
        $xml .= "<AMOUNT_VOID>".$dataX["void"]."</AMOUNT_VOID> \n";
        $xml .= "<AMOUNT_DISCOUNT>".$dataX["discount"]."</AMOUNT_DISCOUNT> \n";
        $xml .= "<AMOUNT_INCREASE>".$dataX["increase"]."</AMOUNT_INCREASE> \n";
        $xml .= "</Z_REPORT>";
        
        break;

      case 4:        
        LogMsg("CRIANDO XML LEITURA X");   

        $dataX        = SelectFiscalStatus($data[$i]["store_key"], $data[$i]["pos_number"], $data[$i]["ticket_number"], $data[$i]["start_time"]);
        $notification = SelectNotification($data[$i]["store_key"], $data[$i]["pos_number"], $data[$i]["ticket_number"],71);
        $nameOper     = $notification["notification_data"];
        if(strpos($nameOper, "Entrada") === FALSE){
          $tempOper     =  explode("]", substr($nameOper, (strpos($nameOper, "Data")+6),strlen($nameOper)));      // saida de operador
          $oper["alternate_id"] = $tempOper[0];
        }else{ 
          $tempOper     =  explode("]", substr($nameOper, (strpos($nameOper, "operador")+8),strlen($nameOper)));  // entrada de operador
          $tempOper_    = SelectMysqlOperName(ltrim($tempOper[0]));
          $oper         = SelectMysqlOper($tempOper_["agent_key"]); 
        }        

        $xml  = "<?xml version='1.0' encoding='ISO-8859-1' ?-> \n";
        $xml .= "<X_REPORT> \n";
        $xml .= "<STORE>".$data[$i]["store_key"]."</STORE><POS>".$data[$i]["pos_number"]."</POS><TICKET>".$data[$i]["ticket_number"]."</TICKET><TRN>".$data[$i]["trn_number"]."</TRN> \n";
        $xml .= "<FISCAL_STORE>".$data[$i]["store_key"]."</FISCAL_STORE><FISCAL_POS>".$data[$i]["pos_number"]."</FISCAL_POS> \n";
        $xml .= "<CASHIER_ID>".$oper["alternate_id"]."</CASHIER_ID> \n";
        $xml .= "<CASHIER_NAME>".$tempOper[0]."</CASHIER_NAME> \n";        
        $xml .= "<FISCAL_DAY>".$data[$i]["fiscal_date_alt"]."</FISCAL_DAY><FISCAL_TIME>".$data[$i]["start_time_alt"]."</FISCAL_TIME> \n";
        $xml .= "<INITIAL_GT>".$dataX["initial_GT"]."</INITIAL_GT> \n";
        $xml .= "<FINAL_GT>".$dataX["final_GT"]."</FINAL_GT> \n";
        $xml .= "<AMOUNT_VOID>".$dataX["void"]."</AMOUNT_VOID> \n";
        $xml .= "<AMOUNT_DISCOUNT>".$dataX["discount"]."</AMOUNT_DISCOUNT> \n";
        $xml .= "<AMOUNT_INCREASE>".$dataX["increase"]."</AMOUNT_INCREASE> \n";
        $xml .= "</X_REPORT>";
        
        break;

      case 32:
        LogMsg("CRIANDO XML VENDA - SALE_TYPE 32");
        
        $dataS= SelectSale($data[$i]["store_key"], $data[$i]["pos_number"], $data[$i]["ticket_number"], $data[$i]["start_time"]); 
        $oper = SelectMysqlOper($dataS["cashier_key"]); 
        
        $xml  = "<?xml version='1.0' encoding='ISO-8859-1' ?-> \n";
        $xml .= "<SALE> \n";
        $xml .= "<STORE>".$data[$i]["store_key"]."</STORE><POS>".$data[$i]["pos_number"]."</POS><TICKET>".$data[$i]["ticket_number"]."</TICKET><TRN>".$data[$i]["trn_number"]."</TRN> \n";
        $xml .= "<FISCAL_STORE>".$data[$i]["store_key"]."</FISCAL_STORE><FISCAL_POS>".$data[$i]["pos_number"]."</FISCAL_POS> \n";
        $xml .= "<CASHIER_ID>".$oper["alternate_id"]."</CASHIER_ID> \n";
        $xml .= "<FISCAL_DAY>".$data[$i]["fiscal_date_alt"]."</FISCAL_DAY><FISCAL_TIME>".$data[$i]["start_time_alt"]."</FISCAL_TIME> \n";
        $xml .= "</SALE>";
        
        break;

      case 47:
        LogMsg("CRIANDO XML RECARGA / CORRBAN");

        $dataS= SelectSale($data[$i]["store_key"], $data[$i]["pos_number"], $data[$i]["ticket_number"], $data[$i]["start_time"]); 
        $oper = SelectMysqlOper($dataS["cashier_key"]); 
        
        $xml  = "<?xml version='1.0' encoding='ISO-8859-1' ?-> \n";
        $xml .= "<SALE> \n";
        $xml .= "<STORE>".$data[$i]["store_key"]."</STORE><POS>".$data[$i]["pos_number"]."</POS><TICKET>".$data[$i]["ticket_number"]."</TICKET><TRN>".$data[$i]["trn_number"]."</TRN> \n";
        $xml .= "<FISCAL_STORE>".$data[$i]["store_key"]."</FISCAL_STORE><FISCAL_POS>".$data[$i]["pos_number"]."</FISCAL_POS> \n";
        $xml .= "<CASHIER_ID>".$oper["alternate_id"]."</CASHIER_ID> \n";
        $xml .= "<FISCAL_DAY>".$data[$i]["fiscal_date_alt"]."</FISCAL_DAY><FISCAL_TIME>".$data[$i]["start_time_alt"]."</FISCAL_TIME> \n";
        $xml .= "</SALE>";
  
        break;

      case 80:
        LogMsg("CRIANDO XML DOACAO");
  
        $dataS= SelectSale($data[$i]["store_key"], $data[$i]["pos_number"], $data[$i]["ticket_number"], $data[$i]["start_time"]); 
        $oper = SelectMysqlOper($dataS["cashier_key"]); 
        
        $xml  = "<?xml version='1.0' encoding='ISO-8859-1' ?-> \n";
        $xml .= "<SALE> \n";
        $xml .= "<STORE>".$data[$i]["store_key"]."</STORE><POS>".$data[$i]["pos_number"]."</POS><TICKET>".$data[$i]["ticket_number"]."</TICKET><TRN>".$data[$i]["trn_number"]."</TRN> \n";
        $xml .= "<FISCAL_STORE>".$data[$i]["store_key"]."</FISCAL_STORE><FISCAL_POS>".$data[$i]["pos_number"]."</FISCAL_POS> \n";
        $xml .= "<CASHIER_ID>".$oper["alternate_id"]."</CASHIER_ID> \n";
        $xml .= "<FISCAL_DAY>".$data[$i]["fiscal_date_alt"]."</FISCAL_DAY><FISCAL_TIME>".$data[$i]["start_time_alt"]."</FISCAL_TIME> \n";
        $xml .= "</SALE>";
  
        break;

      default:
        LogMsg("DESPREZANDO TICKET...: ".$data[$i]["ticket_number"]."  TIPO...: ".$data[$i]["ticket_type"]);
        $record = 0;
        break;
    }
    if($record == 1){
      $pathDir = $ini["BASE_DIR"].$data[$i]["fiscal_date_alt"]."/".str_pad($data[$i]["store_key"], 4, '0',0)."_RESET_XML/";
      CreateDirectory($pathDir, 1);
      $path = $ini["BASE_DIR"].$data[$i]["fiscal_date_alt"]."/".str_pad($data[$i]["store_key"], 4, '0',0)."_RESET_XML/LOJA_".$data[$i]["store_key"]."_PDV_".$data[$i]["pos_number"]."_TICKET_".$data[$i]["ticket_number"].".xml"; 
      $arq = fopen($path, "w");
      fputs($arq, $xml); 
      fclose($arq);
      //sleep(1);
    }
  }
  $files = $pathDir.'*.xml';
  LogMsg("BUSCANDO FILA XML...: $files");
  $contador = 0;
  foreach(glob($files) as $file_name){
    $sFile  = file_get_contents($file_name);
    $aFile  = file($file_name);
    $storeR   = ReadXML($aFile, "<STORE>");
    $posR   = ReadXML($aFile, "<POS>");
    $ticketR = ReadXML($aFile, "<TICKET>");
    $dateR   = ReadXML($aFile, "<FISCAL_TIME>");
    $newfile = $file_name.'_bak';
    copy($file_name, $newfile);
    $CMD = "php -q  integra_con5.php $storeR $posR $ticketR $dateR $file_name ";
    LogMsg("CHAMANDO INTEGRA_CON5.PHP :::::: $CMD");
    $rtn = exec($CMD);
    LogMsg("RETORNO INTEGRA_CON5.PHP ");
    //sleep(1);
  }
}

function RestartIntegration ( $store, $date) {

  global $ini;

  $files = $ini["BASE_DIR"].FormatDate($date,4)."/".str_pad($store, 4, '0',0)."/LOJA_".$store."_PDV_*_DATA*";
  LogMsg("BUSCANDO FILA ARQUIVOS...: $files");
  $contador = 0;
  if(($link = ConnectingInOracle()) == -1){
    LogMsg("FINALIZANDO PROCESSO ::: ERRO ORACLE ");
    exit;
  }else{
    LogMsg("CONECTADO COM SUCESSO NO ORACLE ");
    foreach(glob($files) as $file_name){
      LogMsg("PROCESSANDO ARQUIVO...: $file_name");
      $aFile  = file($file_name);
      foreach($aFile as $line){
        LogMsg("COMANDO...: $line");
        if(strpos($line, "commit") === FALSE){
          $result = InsertOracle($link, 1, $line);
        }else{
          $result = InsertOracle($link, 2);
        }
        LogMsg("RESULTADO DO ORACLE :::::: $result");
      } 
    }
    oci_close($link); 
  }
} 

function ApportionDiscount ($sale, $item) { 
  
  LogMsg (":::::::::::::::::::::::::::::::::::::::::::::");
  LogMsg ("::: ROTINA DE RATEIO DO DESCONTO SUBTOTAL :::");
  LogMsg (":::::::::::::::::::::::::::::::::::::::::::::");
  
  $trib       = array();                                                                                                                             // LISTA DE TRIBUTOS
  $chav       = array();                                                                                                                             // CHAVE DE PESQUISA
  $total_item = 0;                                                                                                                                   // TOTAL DO ITEM
  $c          = 0;                                                                                                                                   // CONTADOR
  $itemOld    = $item;                                                                                                                               // LISTA ITENS ORIGEM
    
  for($z = 0; $z < count($item); $z++){
    // SOLICITACAO DO MARCELO MBR 17/02/16 - NAO APLICAR RATEIO PARA OS CUPONS COM ISS
    /*if(strrpos($item[$z]["pos_id"], "S") !== false){
      LogMsg (":::::::::::::::::::::::::::::::::::::::::::::");
      LogMsg (":::: RATEIO NAO APLICADO - CUPOM COM ISS ::::");
      LogMsg (":::::::::::::::::::::::::::::::::::::::::::::");
      return $item;    
    }*/
    $chave     = $item[$z]["pos_id"];                                                                                                                // TRIBUTO DO ITEM
    $sequence  = $item[$z]["sequence"];                                                                                                              // SEQUENCIA DO ITEM
    $itemCanc  = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 162, $item[$z]["sequence"]); // ITEM CANCELADO  
    if(!array_key_exists($chave, $trib)){
      $trib[$chave]["valor"]          = 0;
      $trib[$chave]["desconto"]       = 0;
      $trib[$chave]["desconto_lido"]  = 0;
      $trib[$chave]["acrescimo"]      = 0;
      $trib[$chave]["acrescimo_lido"] = 0;
      $trib[$chave]["ultimo"]         = 0;
      $chav[$c] = $chave;
      $c++;
    }
    if($item[$z]["voided"] == 0){
      $total_item += $item[$z]["amount"];
      $trib[$chave]["valor"] += $item[$z]["amount"];
    }else{
      $total_item -= $item[$z]["amount"];
      $trib[$chave]["valor"] -= $item[$z]["amount"];
    }
    if($item[$z]["voided"] == 0 and $itemCanc != $item[$z]["sequence"]){
      $trib[$chave]["ultimo"] = $sequence;
    }
  }

  $valor_ult = 0;                                                               // ULTIMO VALOR

  for($z = 0; $z < count($chav); $z++){
    $chave = $chav[$z];
    if($trib[$chave]["valor"] > $valor_ult){
      $valor_ult = $trib[$chave]["valor"];
      $ultimo_trib = $chave;
    }
  }
  
  $total_venda = number_format($sale["amount_due"] - $sale["interest"],2,'.','');
  $total_item  = number_format($total_item,2,'.','');
  $total_ven   = number_format($sale["amount_due"] - $sale["interest"] + $sale["subtotal_discount"],2,'.','');
  $desconto    = $sale["subtotal_discount"];
  $acrescimo   = $sale["increase"];
  
  if($sale["subtotal_discount"] > 0){
    $total_desc = 0;
    for($z = 0; $z < count($trib); $z++){
      $chave = $chav[$z];
      $desc  = number_format(($sale["subtotal_discount"]/100)*($trib[$chave]["valor"]*100/($total_venda+$desconto)),6,'.','');
      $desc  = substr($desc,0,strlen($desc)-4);
      if(($total_desc+$desc) <= $desconto){
        $trib[$chave]["desconto"] = $desc;
        $total_desc += $desc;
      }elseif(($desconto-$total_desc) > 0){
        $trib[$chave]["desconto"] = ($desconto-$total_desc);
        $total_desc += ($desconto-$total_desc);
      }
    }
      
    if(($desconto-$total_desc) > 0){
      $trib[$ultimo_trib]["desconto"] += ($desconto-$total_desc);
    }
    
    for($z = 0; $z < count($item); $z++){
      $chave    = $item[$z]["pos_id"];
      $sequence = $item[$z]["sequence"];
      $rateio   = 0;
      $itemCanc  = SelectAnswerData($sale["store_key"], $sale["pos_number"], $sale["ticket_number"], $sale["start_time"], 162, $item[$z]["sequence"]); // ITEM CANCELADO  
      if($trib[$chave]["ultimo"] == $sequence){
        if(($trib[$chave]["desconto"] - $trib[$chave]["desconto_lido"]) > 0){
          $rateio = ($trib[$chave]["desconto"] - $trib[$chave]["desconto_lido"]);
        }
      }elseif($trib[$chave]["valor"] > 0){
        if(($trib[$chave]["desconto"] - $trib[$chave]["desconto_lido"]) > 0){
          $rateio = ($trib[$chave]["desconto"]/100)*(($item[$z]["amount"] * 100) / $trib[$chave]["valor"]);
          if($rateio > ($trib[$chave]["desconto"] - $trib[$chave]["desconto_lido"])){
            $rateio = ($trib[$chave]["desconto"] - $trib[$chave]["desconto_lido"]);
          }
        }                     
      }   
      $rateio = number_format($rateio,6,'.','');
      $rateio = substr($rateio,0,strlen($rateio)-4);
      if($item[$z]["voided"] == 0 and $itemCanc != $item[$z]["sequence"]){
        $trib[$chave]["desconto_lido"] += $rateio;
        if($rateio < $item[$z]["amount"]){
          $item[$z]["discount"] += $rateio;
          $item[$z]["amount"]   -= $rateio;
        }else{
          while($rateio > 0){
            $w = 0;
            while($w < count($item) and $rateio > 0){
              $sequence = $item[$w]["sequence"];
              if($item[$w]["voided"] == 0 and $itemCanc != $item[$w]["sequence"]){
                if($item[$w]["pos_id"] == $chave){
                    $item[$w]["discount"] += 0.01;
                    $item[$w]["amount"] -= 0.01;
                    $rateio = number_format($rateio-0.01,6,'.','');
                    $rateio = substr($rateio,0,strlen($rateio)-4);
                }
              }
              $w++;
            }
          }
        }
      }
    }
    $total_item -= $sale["subtotal_discount"];
  } 
  $total_item = number_format($total_item,2,'.','');
  for($i = 0; $i < count($item); $i++){
    $msg  = str_pad('ANTES ', 10, ' ',0);
    $msg .= str_pad('ITEM ...: ', 10, ' ',0);
    $msg .= str_pad($itemOld[$i]["plu_id"], 10, ' ',0);
    $msg .= str_pad('SEQUENCIA ...: ', 16, ' ',0);
    $msg .= str_pad($itemOld[$i]["sequence"], 3, '0',0);
    $msg .= str_pad('VALOR ...: ', 12, ' ',0);
    $msg .= str_pad(number_format($itemOld[$i]["amount"],2,'.',''), 7, ' ',0);
    $msg .= str_pad('DESCONTO ...: ', 16, ' ',0);
    $msg .= str_pad(number_format($itemOld[$i]["discount"],2,'.',''), 7, ' ',0);
    LogMsg($msg);
    $msg  = str_pad('DEPOIS ', 10, ' ',0);
    $msg .= str_pad('ITEM ...: ', 10, ' ',0);
    $msg .= str_pad($item[$i]["plu_id"], 10, ' ',0);
    $msg .= str_pad('SEQUENCIA ...: ', 16, ' ',0);
    $msg .= str_pad($item[$i]["sequence"], 3, '0',0);
    $msg .= str_pad('VALOR ...: ', 12, ' ',0);
    $msg .= str_pad(number_format($item[$i]["amount"],2,'.',''), 7, ' ',0);
    $msg .= str_pad('DESCONTO ...: ', 16, ' ',0);
    $msg .= str_pad(number_format($item[$i]["discount"],2,'.',''), 7, ' ',0);
    LogMsg($msg);
  }
  LogMsg (":::::::::::::::::::::::::::::::::::::::::::::");
  LogMsg (":::: FIM ROTINA RATEIO DESCONTO SUBTOTAL ::::");
  LogMsg (":::::::::::::::::::::::::::::::::::::::::::::");
  return $item;
}

function SendEmail($destinatario, $remetente, $assunto, $mensagem, $destinatarioCc) {

  // Dominio do servidor SMTP, para servidores HTTPS coloque o prefixo ssl://
  $servidor = "ssl://email.drogariaminasbrasil.com.br";
  //$servidor = "smtp.remaqbh.com.br";
  // usuario do servidor SMTP
  //$usuario = "suporte#remaqbh.com.br";
  $usuario = "emporium@drogariaminasbrasil.com.br"; 
// senha do servidor SMTP
  //$senha = "Sup0rt3";
  $senha = "Alertas2016";
  // numero da porta do servidor SMTP
  $success = true;
       
  // abro uma conexao por socket com o servidor
  $socket = @fsockopen($servidor, 465, $errno, $errstr, 10);
  if ($socket) {
    $ln = "\r\n"; 
    // pego o texto de retorno do servidor e verifico o codigo da resposta se for 220 ok
    $response = fgets($socket, 465);
    if (!preg_match("/^220/", $response)) $success = false;
    //echo "\n Conectando ao webmail ....:$response \n";
    // envio um comando HELO e verifico o codigo da resposta se for 250 ok
    fwrite($socket, "HELO $servidor".$ln);
    $response = fgets($socket, 465);
    if (!preg_match("/^250/", $response)) $success = false;
    //echo "\n HELO smtp.remaqbh.com.br ....:$response \n";
    // envio um comando AUTH LOGIN para iniciar a autenticacao
    fwrite($socket, "AUTH LOGIN".$ln);
    $response = fgets($socket, 465);
    //echo "\n AUTH LOGIN ....:$response \n";
    // envio o nome de usuario
    fwrite($socket, base64_encode($usuario).$ln);
    fgets($socket, 465);
    //echo "\n USUARIO ....:".base64_encode($usuario)."===".$response."\n";
    // envio senha do servidor
    fwrite($socket, base64_encode($senha).$ln);
    //echo "\n SENHA ....:".base64_encode($senha)."===".$response."\n";
   
    // verifico o codigo da resposta se for 235 a autenticacao ocorreu com sucesso
    $response = fgets($socket, 465);
    if (!preg_match("/^235/", $response)) $success = false;
    //echo "\n AUTH LOGIN ....:$response \n";
   
    // envio um comando MAIL FROM e verifico o codigo da resposta se for 250 ok      
    fwrite($socket, "MAIL FROM:<$remetente>".$ln);
    $response = fgets($socket, 465); 
    if (!preg_match("/^250/", $response)) $success = false;
    //echo "\n MAIL FROM:<$remetente> ....:$response \n";
    
    // envio um comando RCPT TO e verifico o codigo da resposta se for 250 ok 
    fwrite($socket, "RCPT TO:<$destinatario>".$ln);
    $response = fgets($socket, 465);
    if (!preg_match("/^250/", $response)) $success = false;
    
    // envio um comando RCPT TO e verifico o codigo da resposta se for 250 ok 
    if($destinatarioCc){
      fwrite($socket, "RCPT TO:<$destinatarioCc>".$ln);
      $response = fgets($socket, 465);
      if (!preg_match("/^250/", $response)) $success = false;
    }
    // envio um comando RCPT TO e verifico o codigo da resposta se for 250 ok 
    //fwrite($socket, "RCPT TO:<ednilson@remaqbh.com.br>".$ln);
    //$response = fgets($socket, 465);
    //if (!preg_match("/^250/", $response)) $success = false;
    
    // envio um comando DATA e verifico o codigo da resposta se for 354 ok 
    fwrite($socket, "DATA".$ln);
    $response = fgets($socket, 465);
    if (!preg_match("/^354/", $response)) $success = false;
    // definicao dos cabecalhos da mensagem 
    $headers = "Message-Id: <".time().".".md5(microtime())."@>".$ln;
    $headers .= "Date: ".date("r").$ln;
    $headers .= "X-Priority: 3".$ln;
    //$headers .= "Content-Type: text/html; charset=\"UTF-8\"".$ln;
    $headers .= "Content-Type: text/html; charset=\"iso-8859-1\"".$ln;
    $headers .= "Subject: $assunto".$ln;
    $headers .= "To: $destinatario".$ln;       
    if($destinatarioCc){
      $headers .= "Cc: $destinatarioCc".$ln;       
    }
    $headers .= "From: $remetente".$ln.$ln;
    $headers .= $mensagem.$ln.".".$ln;
    fwrite($socket, $headers);
    $response = fgets($socket, 465);
    if (!preg_match("/^250/", $response)) $success = false;
    // envio um comando QUIT e verifico o codigo da resposta se for 221 ok        
    fwrite($socket, "QUIT".$ln);
    $response = fgets($socket, 465);
    if (!preg_match("/^221/", $response)) $success = false;
    // fecho a conexao com o servidor
    fclose($socket);
    return $success;
  }
  else 
  return false;
}


//*****************************************************************
//***************  END FUNCTION CONTROL AND SERVICES **************
//*****************************************************************


//CREATE SQL
/*
CREATE TABLE session_con5
  (
  session_store_key       bigint    (20) unsigned NOT NULL,
  session_pos_number      smallint  (5)  unsigned NOT NULL,
  session_date            date                            ,
  session_number          smallint  (5)  unsigned         ,        
  session_cashier         bigint    (20) unsigned         ,
  PRIMARY KEY (session_store_key, session_pos_number)
  );

CREATE TABLE catalog_con5
  (
  catalog_store_key       bigint    (20) unsigned NOT NULL,
  catalog_pos_number      smallint  (5)  unsigned NOT NULL,
  catalog_ticket_number   bigint    (20) unsigned NOT NULL,  
  catalog_start_time      datetime                NOT NULL,
  catalog_process_time    datetime                        ,
  catalog_status          smallint  (5)  unsigned         ,  
  
  
  PRIMARY KEY (catalog_store_key, catalog_pos_number, catalog_ticket_number, catalog_start_time)
  );

 */
?>
